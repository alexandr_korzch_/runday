package ua.com.pinta.runday.interfaces.models;

import android.location.Location;

import java.util.List;

import ua.com.pinta.runday.classes.run.map.models.PointOnTrack;

/**
 * Created by Cach on 20.05.2015.
 */
public interface ITrack {

    List<PointOnTrack> getTrack();

    void addPoint(Location startLoc, Location curLoc, float distanceM, long timeInSec, float speedMinSec);

    void recalculation();

    float getAverageSpeed();

    float getDistanceInMeters();

    long getTimeInSeconds();


}
