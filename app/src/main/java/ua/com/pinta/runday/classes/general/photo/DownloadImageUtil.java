package ua.com.pinta.runday.classes.general.photo;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;

/**
 * Created by Cach on 02.06.2015.
 */
public class DownloadImageUtil {


    MainActivity activity;

    public DownloadImageUtil(MainActivity activity) {
        this.activity = activity;
    }

    public void downloadFile(String uRl) {

        Log.d("photo", "photo_url - "+uRl);

        String dir = activity.getString(R.string.foto_dir);
        String fileName = activity.getString(R.string.portret_file_name);
        
        File fileForDelete = new File(Environment.getExternalStorageDirectory()+dir+"/"+fileName);
        fileForDelete.delete();


        File direct = new File(Environment.getExternalStorageDirectory()
                + dir);

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager)activity.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Download")
                .setDescription("Download")
                .setDestinationInExternalPublicDir(dir, fileName);

        mgr.enqueue(request);

        Log.d("tag", "file");
        Log.d("tag", Environment.getExternalStorageDirectory()+dir+"/"+fileName);

        mUserProfile.photoInStorage = Environment.getExternalStorageDirectory()+dir+"/"+fileName;
        Log.d("photo", "mUserProfile.photoInStorage - "+mUserProfile.photoInStorage);

    }

}
