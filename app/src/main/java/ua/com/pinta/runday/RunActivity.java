package ua.com.pinta.runday;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ViewGroup;
import ua.com.pinta.runday.classes.general.checkers.ServiceChecker;
import ua.com.pinta.runday.classes.general.checkers.ServiceReceiver;
import ua.com.pinta.runday.classes.run.fragm_collection.FragmentCollection;
import ua.com.pinta.runday.classes.run.navigationdrawer.NavigationDrawerFragment;
import ua.com.pinta.runday.classes.run.tabs.SlidingTabLayout;


public class RunActivity extends AppCompatActivity {

    private void logOut(String message) {
        Log.d("tag", message);
    }

    private MyPagerAdapter pagerAdapter;
    private ViewPager pager;
    public SlidingTabLayout tabsL;
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    public FragmentCollection fragmentCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);
        checkServices();
        fragmentCollection = new FragmentCollection(this);
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        initAdapter(0);
    }


    public void initAdapter(int startPage) {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setActivity(this);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(startPage);

        tabsL = (SlidingTabLayout) findViewById(R.id.tabs);
        tabsL.setViewPager(pager);

        hideLastTab();
    }

    private void hideLastTab() {

        tabsL.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {

                toolbar.setTitle(fragmentCollection.getTitles().get(position));

                if (fragmentCollection.getFragments().size() > 4 && position < 4) {
                    fragmentCollection.removeLast();
                    initAdapter(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }


    class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentCollection.getTitles().get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentCollection.getFragments().get(position);
        }


        @Override
        public int getCount() {
            return fragmentCollection.getFragmentCount();
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);

            if (position == 4) {
                FragmentManager manager = ((Fragment) object).getFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove((Fragment) object);
                trans.commit();
            }

        }

    }

    private void checkServices() {
        ServiceChecker serviceChecker = new ServiceChecker();
        ServiceReceiver serviceController = new ServiceReceiver(this);
        if (!serviceChecker.isOnline(this)) serviceController.connectInternet();
        if (!serviceChecker.isGooglePlayServices(this))
            serviceController.receiveGooglePlayServices();
        if (!serviceChecker.isGPS(this)) serviceController.turnOnGPS();
    }
}
