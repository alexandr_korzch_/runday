package ua.com.pinta.runday.interfaces.map;

/**
 * Created by alex on 10.08.14.
 */
public interface MapCallBack {

    public void onChangLocationNewPolyLines(double lat, double lon);

}
