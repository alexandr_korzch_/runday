package ua.com.pinta.runday.interfaces.map;


import android.location.LocationListener;

/**
 * Created by Cach on 19.05.2015.
 */
public interface ILocation extends LocationListener {

    void settingMyLocation();
}
