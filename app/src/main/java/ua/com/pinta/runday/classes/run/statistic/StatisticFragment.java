package ua.com.pinta.runday.classes.run.statistic;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.run.statistic.callories.CalloriesFragment;
import ua.com.pinta.runday.classes.run.statistic.distance.DistanceFragment;


public class StatisticFragment extends Fragment {

    public static StatisticFragment newInstance() {
        StatisticFragment fragment = new StatisticFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public StatisticFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_statistic, container, false);

        final DistanceFragment distanceFragment = DistanceFragment.newInstance();
        final CalloriesFragment calloriesFragment = CalloriesFragment.newInstance();

        final FragmentTransaction[] fTrans = new FragmentTransaction[1];
        fTrans[0] = getChildFragmentManager().beginTransaction();
        fTrans[0].add(R.id.statistic_container, distanceFragment);
        fTrans[0].commit();


        FrameLayout distance_frame = (FrameLayout) v.findViewById(R.id.distance_frame);
        FrameLayout callories_frame = (FrameLayout) v.findViewById(R.id.callories_frame);

        distance_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans[0] = getChildFragmentManager().beginTransaction();
                fTrans[0].replace(R.id.statistic_container, distanceFragment);
                fTrans[0].commit();

            }
        });


        callories_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans[0] = getChildFragmentManager().beginTransaction();
                fTrans[0].replace(R.id.statistic_container, calloriesFragment);
                fTrans[0].commit();


            }
        });

        return v;
    }
}
