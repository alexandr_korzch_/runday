//package ua.com.pinta.runday.classes.general.dao;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.SQLException;
//import android.database.sqlite.SQLiteDatabase;
//import android.util.Log;
//import java.util.ArrayList;
//import java.util.List;
//import ua.com.pinta.runday.classes.run.map.models.PointOnTrack;
//import ua.com.pinta.runday.interfaces.dao.ITrackDB;
//
///**
// * Created by Cach on 20.05.2015.
// */
//public class TrackDB implements ITrackDB {
//
//    private Context context;
//    private DBHelper dbHelper;
//    private SQLiteDatabase database;
//    private Cursor cursor;
//
//    public TrackDB(Context context) {
//        logOut("new TrackDB");
//        this.context = context;
//        dbHelper = new DBHelper(context);
//
//    }
//
//    @Override
//    public void saveTrack(List<PointOnTrack> points) {
//        logOut("saveTrackDB()");
//        logOut("points - "+points);
//        logOut("points.size - "+points.size());
//
//        openDB();
//
//        for(PointOnTrack point: points){
//
//            logOut("SAVE");
//            logOut("track_number"+ point.getTrack());
//            logOut("lat"+ point.getLat());
//            logOut("lon"+ point.getLon());
//
//            ContentValues row = new ContentValues();
//
//            Log.d("tag","track_number - "+point.getTrack()+"");
//            row.put("track_number", point.getTrack());
//            row.put("lat", point.getLat());
//            row.put("lon", point.getLon());
//
//            database.insert("track", null, row);
//
//        }
//
//        closeDB();
//    }
//
//
//
//    @Override
//    public List<PointOnTrack> getLastTrack() {
//        logOut("getLastTrack()");
//
//        List<PointOnTrack> track  = new ArrayList<PointOnTrack>();
//        openDB();
//        cursor = database.rawQuery("SELECT * FROM track WHERE track_number = (SELECT max(track_number) FROM track)", null);
//        cursor.moveToFirst();
//
//        while (!cursor.isAfterLast()) {
//
//            logOut("GET");
//            logOut("" + cursor.getInt(cursor.getColumnIndex("track_number")));
//            logOut(""+cursor.getDouble(cursor.getColumnIndex("lat")));
//            logOut("" + cursor.getDouble(cursor.getColumnIndex("lon")));
//
//            PointOnTrack point = new PointOnTrack();
//
//            point.setTrack(cursor.getInt(cursor.getColumnIndex("track_number")));
//            point.setLat(cursor.getDouble(cursor.getColumnIndex("lat")));
//            point.setLon(cursor.getDouble(cursor.getColumnIndex("lon")));
//
//            track.add(point);
//
//            cursor.moveToNext();
//        }
//
//        closeDB();
//
//        return track;
//    }
//
//
//
//
//    @Override
//    public int getNewTrackNumber() {
//        logOut("getLastTrackNumberDB()");
//        openDB();
//        cursor = database.rawQuery("SELECT max(track_number) FROM track", null);
//        cursor.moveToFirst();
//        int lastTrack = cursor.getInt(cursor.getColumnIndex("track_number"));
//        closeDB();
//        logOut("lastTrackFromDB - "+lastTrack);
//        return ++lastTrack;
//    }
//
//
//    private void openDB() throws SQLException {
//        //Log.d("tag", "openDB()");
//        try {
//            database = dbHelper.getWritableDatabase();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    private void closeDB() {
//        //Log.d("tag", "closeDB()");
//        try {
//            if (database != null && database.isOpen())
//                dbHelper.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void logOut(String message){
//        Log.d("tag", message);
//      //  mapActivity.map_textView.append(message + "\n");
//    }
//
//}
