package ua.com.pinta.runday.classes.general.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import ua.com.pinta.runday.interfaces.dao.IDBHelper;


/**
 * Created by Cach on 15.05.2015.
 */
public class DBHelper extends SQLiteOpenHelper implements IDBHelper {

    private static final String DB_NAME = Environment.getExternalStorageDirectory().toString() + "/RunDay/DB/runner.sqlite";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 6);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
