package ua.com.pinta.runday.interfaces.views;

/**
 * Created by alexandr on 16.05.15.
 */
public interface IViewRouter {

    void showWelcome();
    void showEnter();
    void showRegistration();
    void showProfile();

//    void showPager();

    void showPreviousView();


}
