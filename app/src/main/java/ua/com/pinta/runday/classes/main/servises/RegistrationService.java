package ua.com.pinta.runday.classes.main.servises;


import android.os.Message;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.classes.general.constatnts.Variables;
import ua.com.pinta.runday.interfaces.servises.IRegistrationService;

/**
 * Created by alexandr on 16.05.15.
 */
public class RegistrationService implements IRegistrationService {

    MainActivity activity;

    String email;
    String password;
    String password_confirmation;
    Long user_Id;

    String token;

    private final HttpClient client = new DefaultHttpClient();
    private String content;

    public RegistrationService(MainActivity activity) {
        if(activity.isLog()) Log.d("tag", "new RegistrationService()");
        this.activity = activity;
    }

    @Override
    public void registration(String email, String password, String password_confirmation) {

        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;

        if(activity.isLog()) Log.d("tag", "registration(String email, String password, String password_confirmation)");
        if(activity.isLog()) Log.d("tag", "email - "+email);
        if(activity.isLog()) Log.d("tag", "password - "+password);
        if(activity.isLog()) Log.d("tag", "password_confirmation - "+password_confirmation);

        doInNewThread("http://runner.pixy.pro/api/v1/singup");

    }

    protected void doInNewThread(final String... urls) {

        if(activity.isLog()) Log.d("tag", "doInNewThread(final String... urls)");

        Thread t = new Thread(new Runnable() {
            public void run() {

                try {

                    HttpPost httppost = new HttpPost(urls[0]);
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                    nameValuePairs.add(new BasicNameValuePair("email", email));
                    nameValuePairs.add(new BasicNameValuePair("password", password));
                    nameValuePairs.add(new BasicNameValuePair("password_confirmation", password_confirmation));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    ResponseHandler responseHandler = new BasicResponseHandler();

                    content = (String) client.execute( httppost, responseHandler );
                    Log.d("tag", "content - "+content);


                } catch (ClientProtocolException e) {

                    e.printStackTrace();
                    Log.d("tag", "ClientProtocolException - " + e);

                } catch (IOException e) {

                    e.printStackTrace();
                    Log.d("tag", "IOException e - " + e);

                }catch (IllegalArgumentException e){

                    e.printStackTrace();
                    Log.d("tag", "IllegalArgumentException - " + e);
                    return;

                }

                if(content != null)ParsJSON(content);

            }
        });
        t.start();

    }

    protected void ParsJSON(String result) {

        if(activity.isLog()) Log.d("tag", "ParsJSON(String content)");
        if(activity.isLog()) Log.d("tag", "result - " + result);

        try {

            JSONObject response = new JSONObject(result);

            if(response.getString("status").equalsIgnoreCase("success")
                    && response.getString("profile").equalsIgnoreCase("empty")) {

                token = response.getString("token");
                Variables.token = token;

                if(activity.isLog()) Log.d("tag", "Regiistration token - " + token);

                writeNewUserToDB();

            }else {
                if(activity.isLog()) Log.d("tag", "!result");
                showRegistration(false);
            }

        } catch (JSONException e1) {
            if(activity.isLog()) Log.d("tag", ""+e1);
            showRegistration(false);
        }catch (NullPointerException en){
            if(activity.isLog()) Log.d("tag", "" + ""+en);
            showRegistration(false);
        }

    }


    void writeNewUserToDB(){

        activity.getBeanContainer(false).getLoginDB(false).writeNewUserEmail(email);

        showRegistration(true);
        showProfile();

    }



    private void showRegistration(boolean success){

        Message msg;
        if(success){
            msg = activity.getHandler().obtainMessage(1, "Регистрация прошла успешно");
            activity.getHandler().sendMessage(msg);
        }else{
            msg = activity.getHandler().obtainMessage(1, "Регистрация не удалась, \nпожалуйста повторите \nпопытку регистрации");
            activity.getHandler().sendMessage(msg);

        }
    }

    private void showProfile(){

        Message msg = activity.getHandler().obtainMessage(2, "profile");
        activity.getHandler().sendMessage(msg);

    }
}
