package ua.com.pinta.runday.classes.general.checkers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import ua.com.pinta.runday.interfaces.checkers.IServiceReceiver;


/**
 * Created by alexandr on 16.05.15.
 */
public class ServiceReceiver implements IServiceReceiver {

    //MainActivity activity;
    Context context;

    public ServiceReceiver(Context context) {
        this.context = context;
    }

    @Override
    public void connectInternet() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        String internet = "Для работы приложения необходимо подключить интернет";
        alertDialog.setTitle("Настройки интернет");
        alertDialog.setMessage(internet);


            String yes = "Да";
            alertDialog.setPositiveButton(yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));

                }
            });

            String no = "Нет";
            alertDialog.setNegativeButton(no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });

            alertDialog.show();

    }

    @Override
    public void receiveGooglePlayServices() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        String internet = "Для работы приложения необходимо скачать и установить GooglePlayServices";
        alertDialog.setTitle("Настройки GooglePlayServices");
        alertDialog.setMessage(internet);

        String yes = "Да";
        alertDialog.setPositiveButton(yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent serviceIntent = new Intent(Intent.ACTION_VIEW);
                serviceIntent.setData(Uri.parse("market://details?id=com.google.android.gms"));
                context.startActivity(serviceIntent);

            }
        });

        String no = "Нет";
        alertDialog.setNegativeButton(no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        alertDialog.show();;

    }

    @Override
    public void turnOnGPS() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Настройки GPS");
        alertDialog.setMessage("GPS не включен, включить?");

        alertDialog.setPositiveButton("Включить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });


        alertDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

}
