package ua.com.pinta.runday.interfaces.checkers;

import android.app.Activity;

/**
 * Created by alexandr on 16.05.15.
 */
public interface IServiceChecker {

    boolean isOnline(Activity activity);

    boolean isGooglePlayServices(Activity activity);

    boolean isGPS(Activity activity);

}
