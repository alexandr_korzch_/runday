package ua.com.pinta.runday.classes.run.map;

import android.location.Location;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.run.map.popup.PopupHelper;

/**
 * Created by alex on 10.08.14.
 */
public class DrawOnMap  {


    MapFragment mapFragment;

    public DrawOnMap(MapFragment mapFragment) {
        this.mapFragment = mapFragment;
    }

    public void addMarkerToMap(double lat, double lon) {
        logOut("addMarker");
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, lon)).title("My Location");
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        mapFragment.mMap.addMarker(marker);
    }


    public void addPoliline(Location startLoc, Location endLoc, float speed){//speed m/sec
        logOut("addPoliline()");
        Polyline line = mapFragment.mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(startLoc.getLatitude(), startLoc.getLongitude()), new LatLng(endLoc.getLatitude(), endLoc.getLongitude()))
                .width(5));
        if(speed > 0 && speed <= 1.666666666667)line.setColor(mapFragment.getResources().getColor(R.color.one));// 6 km/h
        if(speed > 1.666666666667)line.setColor(mapFragment.getResources().getColor(R.color.seven));
    }


    public void showPopup(){

        PopupWindow window = PopupHelper.newBasicPopupWindow(mapFragment.getActivity());
        LayoutInflater inflater = (LayoutInflater) mapFragment.getActivity().getSystemService(mapFragment.getActivity().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup, null);
        window.setContentView(popupView);

        DisplayMetrics metrics = new DisplayMetrics();
        mapFragment.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        window.setWidth(metrics.widthPixels);

        TextView speedText = (TextView)popupView.findViewById(R.id.speed_textView);
        TextView timeText = (TextView)popupView.findViewById(R.id.time_textView);
        TextView distanceText = (TextView)popupView.findViewById(R.id.distance_textView);

        speedText.setText(Float.toString(mapFragment.track.getAverageSpeed()).substring(0,3)+" m/s");
        timeText.setText((mapFragment.track.getTimeInSeconds()/60+"."
                +mapFragment.track.getTimeInSeconds()%60)
                .substring(0,3)+ " min");
        distanceText.setText(Float.toString(mapFragment.track.getDistanceInMeters()).substring(0,1)+" m");

        int totalHeight = mapFragment.getActivity().getWindowManager().getDefaultDisplay().getHeight();
        int[] location = new int[2];
        mapFragment.tabsL.getLocationOnScreen(location);
        if (location[1] < (totalHeight / 2.0)) {
            window.setAnimationStyle(R.style.Animations_GrowFromTop);
            window.showAsDropDown(mapFragment.tabsL);
        } else {
            PopupHelper.showLikeQuickAction(window, popupView, mapFragment.tabsL, mapFragment.getActivity().getWindowManager(), 0, 0);
        }
    }


    private void logOut(String message){
        Log.d("tag", message);
        //mapActivity.map_textView.append(message + "\n");
    }
}