package ua.com.pinta.runday.classes.general.files;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.files.IWorkWithFiles;


/**
 * Created by Cach on 15.05.2015.
 */
public class WorkWithFiles implements IWorkWithFiles {

    private MainActivity activity;

    String dir_photo;
    String dir_db;
    String file_name;

    public WorkWithFiles(MainActivity activity) {
        this.activity = activity;
        if(activity.isLog())Log.d("tag", " new WorkWithFiles");

        dir_photo = activity.getString(R.string.foto_dir);
        dir_db = activity.getString(R.string.db_dir);
        file_name = activity.getString(R.string.db_file_name);

        Log.d("tag","dir_db - "+dir_db);
        Log.d("tag","dir_photo - "+dir_photo);
    }



    @Override
    public void createFolders() {

        if(activity.isLog())Log.d("tag", "createFolders()");

        File db = new File(Environment.getExternalStorageDirectory()+dir_db);

        if(!db.exists()){
            db.mkdirs();
            if(activity.isLog())Log.d("tag", db.toString());
            if(activity.isLog())Log.d("tag", "mkdirs()");
        }else{

            if(activity.isLog())Log.d("tag", "!mkdirs()");

        }

        File dir = new File(Environment.getExternalStorageDirectory()+dir_photo);

        if(!dir.exists())dir.mkdirs();

//        File screens = new File(Environment.getExternalStorageDirectory()+"/runday/MapScreens");
//
//        if(!screens.exists())screens.mkdir();

    }

    @Override
    public void copyAssets() {

        if(activity.isLog())Log.d("tag", "copyAssets()");

        if((!new File(Environment.getExternalStorageDirectory().toString() + dir_db + "/"+ file_name).exists())){

            AssetManager assetManager = activity.getAssets();
            String[] files = null;

            try {
                files = assetManager.list("");

            } catch (IOException e) {


            }


            for (String filename : files) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(filename);
                    File outFile = new File(Environment.getExternalStorageDirectory().toString() + dir_db, filename);
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;

                } catch (IOException e) {

                }
            }

        }else{

            if(activity.isLog())Log.d("tag", "!copyAssets()");
            return;
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
