package ua.com.pinta.runday;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import ua.com.pinta.runday.classes.main.networks.SocialNetworksRegFragment;


public class NetworksActivity extends ActionBarActivity implements FragmentManager.OnBackStackChangedListener {

    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
    private static ProgressDialog pd;
    static Context context;
    SocialNetworksRegFragment socialNetworksRegFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networks);
        context = this;
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        homeAsUpByBackStack();

        socialNetworksRegFragment = new SocialNetworksRegFragment();
        socialNetworksRegFragment.setNetId((int) getIntent().getExtras().get("networkId"));
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, socialNetworksRegFragment)
                    .commit();
        }


    }


    @Override
    public void onBackStackChanged() {
        homeAsUpByBackStack();
    }

    private void homeAsUpByBackStack() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
//        if (backStackEntryCount > 0) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        } else {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        }
    }

//    public static void showProgress(String message) {
//        pd = new ProgressDialog(context);
//        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        pd.setMessage(message);
//        pd.setCancelable(false);
//        pd.setCanceledOnTouchOutside(false);
//        pd.show();
//    }
//
//    public static void hideProgress() {
//        pd.dismiss();
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, MainActivity.class);
        this.setResult(this.RESULT_OK, intent);
        this.finish();
    }
}
