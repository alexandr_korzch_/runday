package ua.com.pinta.runday;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.classes.general.photo.Decoder;
import ua.com.pinta.runday.classes.general.photo.DownloadImageUtil;
import ua.com.pinta.runday.classes.main.rootcontainer.BeanContainer;


public class MainActivity extends Activity {

    private BeanContainer beanContainer;
    private Handler handler;
    private boolean log = true;

    final int TOAST = 1;
    final int VIEW = 2;

    public static final int REQUEST_NETWORKS = 1;
    public static final int CAMERA = 2;
    public static final int GALLERY = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        printFingerPrintForVK();
//        printHasKeyForFaceBook();

        Log.d("tag", "Start");
        Log.d("tag", "onCreate");

        beanContainer = getBeanContainer(false);
        beanContainer.getWorkWithFiles(false).createFolders();
        beanContainer.getWorkWithFiles(false).copyAssets();
        beanContainer.getViewRouter(false).showWelcome();
        initHandler();

    }


    private void initHandler() {
        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {

                switch (msg.what) {
                    case TOAST:
                        Toast.makeText(getApplicationContext(), (String) msg.obj, Toast.LENGTH_LONG).show();
                        break;
                    case VIEW:

                        if (msg.obj.equals("map")) {
                            goToMapActivity();
                            break;
                        } else {
                            getBeanContainer(false).getShowView(false, (String) msg.obj).show();

                        }

                        if (msg.obj.equals("registration")) {
                            goToRegistration();
                            break;
                        }
                        break;
                }

            }

            ;

        };
    }

    private void goToRegistration() {
        getBeanContainer(false).getViewRouter(false).showRegistration();
    }

    public Handler getHandler() {
        return handler;
    }

    public BeanContainer getBeanContainer(boolean newObj) {
        if (newObj) return new BeanContainer(this);
        if (beanContainer == null) beanContainer = new BeanContainer(this);
        return beanContainer;
    }

    public boolean isLog() {
        return log;
    }


    private void goToMapActivity() {
        Intent mapactivityIntent = new Intent(this, RunActivity.class);
        startActivity(mapactivityIntent);

    }

    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == REQUEST_NETWORKS) {
            // здесь получаем результат из соцсети

            /*скачиваем фото*/
            new DownloadImageUtil(this).downloadFile(mUserProfile.avatar);
            Log.d("photo",mUserProfile.avatar+"");


        /*заполняем меил*/
            EditText email_enter_edit = (EditText) findViewById(R.id.email_enter_edit);
            if (mUserProfile.email != null && !mUserProfile.email.equals("null")) {
                email_enter_edit.setText(mUserProfile.email);
            }
        }

        if (requestCode == CAMERA) {
            String mCurrentPhotoPath;
            int compressPhotoInPercent = 20;//0-100

            /*Получаем картинку, сжимаем до compressPhotoInPercent*/
            Bitmap imageBitmap = getAndSaveBitmap(data);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, compressPhotoInPercent, bytes);

            /*Сохраняем фото*/
            File imageFile = new File(Environment.getExternalStorageDirectory() + "/Runday/Photo/" + "portret" + ".jpg");
            try {
                imageFile.createNewFile();
                FileOutputStream fo = new FileOutputStream(imageFile);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*поворачиваем фото из галлереи*/
            mUserProfile.bitmap = imageBitmap;

            /*показываем картинку*/
            getBeanContainer(false).getViewRouter(false).showProfile();
//            photo_imageView.setImageBitmap(imageBitmap);

            /*Выводим в лог путь к фото*/
            mCurrentPhotoPath = "" + imageFile.getAbsolutePath();
            mUserProfile.photoInStorage = mCurrentPhotoPath;
            Log.d("tag", "PhotoPath - " + mCurrentPhotoPath);
        }




        if (requestCode == GALLERY) {

            /*показываем картинку*/
            Bitmap imageBitmap = null;
            try {
                imageBitmap = new Decoder().decodeURI(data.getStringExtra("pathToPhoto"));
                Log.d("tag", "PhotoGallery - " + data.getStringExtra("pathToPhoto"));
                Log.d("tag", "PhotoGallery - " + data.getStringExtra("pathToPhoto"));

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            mUserProfile.bitmap = new Decoder().rotateBitmap(imageBitmap, 90.0f);
//            mUserProfile.bitmap = imageBitmap;

            /*показываем картинку*/
            getBeanContainer(false).getViewRouter(true).showProfile();
//            photo_imageView.setImageBitmap(imageBitmap);

            /*Выводим в лог путь к фото*/
            if (data.getStringExtra("pathToPhoto") != null) {
                mUserProfile.photoInStorage = data.getStringExtra("pathToPhoto");
                Log.d("tag", "PhotoPathFromGallery - " + data.getStringExtra("pathToPhoto"));
            }

        }

    }

    private Bitmap getAndSaveBitmap(Intent data){

        Bitmap imageBitmap = null;
        try {
            imageBitmap = (Bitmap) data.getExtras().get("data");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mUserProfile.bitmap = imageBitmap;

        return imageBitmap;

    }


    private void logOut(String message) {

        Log.d("tag", message + "");

    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Быстрое двойное нажатие закроет программу", Toast.LENGTH_SHORT).show();

        getBeanContainer(false).getViewRouter(false).showPreviousView();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 1000);
    }


//    //time
//    private void printHasKeyForFaceBook() {
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "ua.com.pinta.runday",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("tag", "HashKeyForFaceBook");
//                Log.d("tag", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                Log.d("tag", "\n");
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//        }
//    }
//
//    //time
//    private void printFingerPrintForVK(){
//        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
//        for(String fingerprint: fingerprints){
//            Log.d("tag", "fingerprintForVK  - " + fingerprint);
//        }
//    }

}
