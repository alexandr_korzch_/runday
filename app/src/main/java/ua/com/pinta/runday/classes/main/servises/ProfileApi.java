package ua.com.pinta.runday.classes.main.servises;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Cach on 03.06.2015.
 */
public interface ProfileApi {

    @Multipart
    @POST("/api/v1/save_profile")
    void sendProfile(
            @Query("token") String token,
            @Query("firstname") String firstname,
            @Query("sex") String sex,
            @Query("age") String age,
            @Query("height") String height,
            @Query("weight") String weight,
            @Query("country") String country,
            @Query("city") String city,
            @Part("avatar") TypedFile photo,
            Callback<Response> response);

}
