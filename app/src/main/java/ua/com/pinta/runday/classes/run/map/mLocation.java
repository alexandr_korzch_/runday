package ua.com.pinta.runday.classes.run.map;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import ua.com.pinta.runday.interfaces.map.ILocation;

/**
 * Created by Cach on 19.05.2015.
 */
public class mLocation implements ILocation {

    private MapFragment mapFragment;
    private boolean first_point = true;
    private Location oldLocation;
    private Location currentLocation;
    private long oldTimeInMillis;
    private long currentTimeInMillis;

    public mLocation(MapFragment mapFragment) {
        Log.d("tag", "new mLocation()");
        this.mapFragment = mapFragment;
    }


    @Override
    public void settingMyLocation() {
        logOut("settingMyLocation()");

        LocationManager locationManager = (LocationManager)mapFragment.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String provider = locationManager.getBestProvider(criteria, true);

        if(provider != null)locationManager.requestLocationUpdates(provider, 1000, 1f, this);//����� ���������� � �������������- long

    }

    @Override
    public void onLocationChanged(android.location.Location location) {

        logOut("onLocationChanged()");

        mapFragment.animCamera(location.getLatitude(), location.getLongitude(), 18, 0, 0);

        if(first_point){
            oldLocation = location;
            currentLocation = location;
            oldTimeInMillis = location.getTime();
            currentTimeInMillis = location.getTime();
            mapFragment.drawOnMap.addMarkerToMap(location.getLatitude(), location.getLongitude());
            mapFragment.run__map_button.setEnabled(true);
            first_point = false;
        }else{
            oldLocation = currentLocation;
            currentLocation = location;
            oldTimeInMillis = currentTimeInMillis;
            currentTimeInMillis = location.getTime();
        }

        if(mapFragment.recordTrack){
            logOut("recordPoint");

            float distanceInMeter = location.distanceTo(oldLocation);//float
            long timeInSec = (currentTimeInMillis - oldTimeInMillis)/1000;//long
            float speedMeterInSec = distanceInMeter/timeInSec;//float

            mapFragment.track.addPoint(
                    oldLocation,
                    currentLocation,
                    distanceInMeter,
                    timeInSec,
                    speedMeterInSec);

            mapFragment.drawOnMap.addPoliline(
                    oldLocation,
                    currentLocation, speedMeterInSec);

        } else {
            logOut("!recordTrack");
        }
    }



    public Location getMyLocation() {
        return currentLocation;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        logOut("onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        logOut("onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        logOut("onProviderDisabled");
    }

    private void logOut(String message){
        Log.d("tag", message);
    }
}
