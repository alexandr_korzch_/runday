package ua.com.pinta.runday.classes.main.views;

import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.listeners.ILoginListener;
import ua.com.pinta.runday.interfaces.views.IShow;


/**
 * Created by alexandr on 16.05.15.
 */
public class ShowLoginView implements IShow {

    MainActivity activity;

    TextView remember_password_text;
    TextView enter_text;

    ImageView facebook_imageView;
    ImageView twitter_imageView;
    ImageView google_imageView;
    ImageView vk_imageView;

    EditText email_enter_edit;
    EditText password_enter_edit;


    public ShowLoginView(MainActivity activity) {
        if(activity.isLog()) Log.d("tag", "new ShowLoginView()");
        this.activity = activity;

    }

    @Override
    public void show() {
        if(activity.isLog()) Log.d("tag", "showEnter()");
        activity.setContentView(R.layout.enter);
        init();


    }

    private void init(){

        if(activity.isLog()) Log.d("tag", "init()");

        remember_password_text = (TextView)activity.findViewById(R.id.rememberpassword_text);
        enter_text = (TextView)activity.findViewById(R.id.enter_text);

        facebook_imageView = (ImageView)activity.findViewById(R.id.facebook_imageView);
        twitter_imageView = (ImageView)activity.findViewById(R.id.twitter_imageView);
        google_imageView = (ImageView)activity.findViewById(R.id.google_imageView);
        vk_imageView = (ImageView)activity.findViewById(R.id.vk_imageView);

        ILoginListener enterListener = activity.getBeanContainer(false).getLoginListener(false);

        if(activity.isLog()) Log.d("tag", "remember_password_text - "+remember_password_text);
        if(activity.isLog()) Log.d("tag", "enterListener - "+enterListener);

        remember_password_text.setOnClickListener(enterListener);
        enter_text.setOnClickListener(enterListener);

        facebook_imageView.setOnClickListener(enterListener);
        twitter_imageView.setOnClickListener(enterListener);
        google_imageView.setOnClickListener(enterListener);
        vk_imageView.setOnClickListener(enterListener);

        email_enter_edit = (EditText)activity.findViewById(R.id.email_enter_edit);
        password_enter_edit = (EditText)activity.findViewById(R.id.password_enter_edit);

        showEmail();

    }

    private void showEmail(){
        if(activity.isLog()) Log.d("tag", "showEmail()");
        email_enter_edit.setText(
                activity.getBeanContainer(false)
                        .getLoginDB(false)
                        .getUserEmail());

    }


}
