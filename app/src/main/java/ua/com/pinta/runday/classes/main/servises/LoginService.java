package ua.com.pinta.runday.classes.main.servises;

import android.os.Message;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.general.constatnts.Variables;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.interfaces.servises.ILoginService;

/**
 * Created by alexandr on 17.05.15.
 */
public class LoginService implements ILoginService {

    MainActivity activity;

    String email;
    String password;

    String token;
    String server;

    private final HttpClient client = new DefaultHttpClient();
    private String content;

    public LoginService(MainActivity activity) {
        if(activity.isLog()) Log.d("tag", "new LoginService()");
        this.activity = activity;
        server = activity.getString(R.string.server);
    }

    @Override
    public void login(String email, String password) {
        if(activity.isLog()) Log.d("tag", "enter("+ email+", "+password+");");

        this.email = email;
        this.password = password;

        doInNewThread("http://runner.pixy.pro/api/v1/login");

    }

    private void doInNewThread(final String... urls) {

        if(activity.isLog()) Log.d("tag", "doInNewThread(final String... urls)");

        Thread t = new Thread(new Runnable() {
            public void run() {

                try {

                    HttpPost httppost = new HttpPost(urls[0]);
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                    nameValuePairs.add(new BasicNameValuePair("email", email));
                    nameValuePairs.add(new BasicNameValuePair("password", password));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    ResponseHandler responseHandler = new BasicResponseHandler();

                    content = (String) client.execute( httppost, responseHandler );

                } catch (ClientProtocolException e) {

                    e.printStackTrace();

                } catch (IOException e) {

                    e.printStackTrace();

                }catch (IllegalArgumentException e){

                    e.printStackTrace();
                    return;

                }

                ParsJSON(content);

            }
        });
        t.start();

    }

    protected void ParsJSON(String result) {

        if(activity.isLog()) Log.d("tag", "ParsJSON(String result)");
        if(activity.isLog()) Log.d("tag", "result - " + result);

        try {
            JSONObject response = new JSONObject(result);

            if(response.getString("status").equalsIgnoreCase("failed")){
                if(activity.isLog()) Log.d("tag", "status - failed");

                showRegistration(false);

            }else if(response.getString("status").equalsIgnoreCase("success")) {
                if(activity.isLog()) Log.d("tag", "status - success");

                try {
                    if (activity.isLog()) Log.d("tag", "JSONObject profile = response.getJSONObject(profile);");
                    JSONObject profile = response.getJSONObject("profile");
                    Log.d("tag", "profile - " + profile);

                    token = response.getString("token");
                    Variables.token = token;
                    if (activity.isLog()) Log.d("tag", "Login token - " + token);

                    User user = activity.getBeanContainer(false).getUser(false);
                    user.setEmail(email);
                    user.setFirstName(profile.getString("firstname"));
                    user.setSex(profile.getString("sex"));
                    user.setAge(profile.getInt("age"));
                    user.setHeight(profile.getInt("height"));
                    user.setWeight(profile.getInt("weight"));
                    user.setCountry(profile.getString("country"));
                    user.setCity(profile.getString("city"));
                    user.setFoto(server + profile.getString("foto"));


                    if(activity.isLog()) Log.d("tag", "end pars");
                    saveProfileUserToDB(user);

                    /*добавляем в профайл фото*/
                    mUserProfile.avatar = user.getFoto();
                    if(activity.isLog()) Log.d("tag", "mUserProfile.avatar - "+mUserProfile.avatar);


                    return;
                } catch (JSONException e) {
                    if (activity.isLog()) Log.d("tag", "profile is Object, not String");
                }

                    String profileStr = response.getString("profile");
                    if (profileStr.equalsIgnoreCase("empty")) ;
                    token = response.getString("token");
                    Variables.token = token;
                    if (activity.isLog()) Log.d("tag", "Login token - " + token);
                    writeNewUserToDB();

            }

//        } catch (ParseException e) {
//            if (activity.isLog()) Log.d("tag", "ParseException - " + e);
        } catch (JSONException e) {
            if (activity.isLog()) Log.d("tag", "JSONException - " + e);
        }catch (NullPointerException e){
            if (activity.isLog()) Log.d("tag", "NullPointerException - " + e);
        }
    }

    private void writeNewUserToDB(){
        User user = activity.getBeanContainer(false).getUser(true);
        user.setEmail(email);

        activity.getBeanContainer(false).getLoginDB(false).writeNewUserEmail(email);
        showRegistration(true);
        showProfile();
    }



    private void saveProfileUserToDB(User user){
        activity.getBeanContainer(false).getProfileDB(false).updateProfileInDB(user);
        showProfile();
    }


    private void showRegistration(boolean success){
        Message msg;
        if(success){
            msg = activity.getHandler().obtainMessage(1, "Вход прошел успешно, данные обновлены");
            activity.getHandler().sendMessage(msg);
        }else{
            msg = activity.getHandler().obtainMessage(1, "Войти не удалось, \nпожалуйста зарегестрируйтесь");
            activity.getHandler().sendMessage(msg);
            msg = activity.getHandler().obtainMessage(2, "registration");
            activity.getHandler().sendMessage(msg);
        }
    }


    private void showProfile(){
        Message msg = activity.getHandler().obtainMessage(2, "profile");
        activity.getHandler().sendMessage(msg);
    }
}
