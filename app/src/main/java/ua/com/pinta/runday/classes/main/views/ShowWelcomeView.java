package ua.com.pinta.runday.classes.main.views;

import android.util.Log;
import android.widget.Button;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.listeners.IWelcomeListener;
import ua.com.pinta.runday.interfaces.views.IShow;


/**
 * Created by alexandr on 16.05.15.
 */
public class ShowWelcomeView implements IShow {

    private MainActivity activity;

    public ShowWelcomeView(MainActivity activity) {
        this.activity = activity;
        if(activity.isLog())Log.d("tag", "new ShowWelcomeView()");
    }

    @Override
    public void show() {

        if(activity.isLog())Log.d("tag", "showWelcome()");

        activity.setContentView(R.layout.welcome);

        Button enter = (Button)activity.findViewById(R.id.enter_bt);
        Button registration = (Button)activity.findViewById(R.id.reg_bt);

        IWelcomeListener welcomeListener = activity.getBeanContainer(false).getWelcomeListener(false);

        enter.setOnClickListener(welcomeListener);
        registration.setOnClickListener(welcomeListener);

    }
}
