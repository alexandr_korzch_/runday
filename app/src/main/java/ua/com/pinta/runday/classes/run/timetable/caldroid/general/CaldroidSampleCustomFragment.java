package ua.com.pinta.runday.classes.run.timetable.caldroid.general;


import ua.com.pinta.runday.classes.run.timetable.caldroid.library.fragment.CaldroidFragment;

public class CaldroidSampleCustomFragment extends CaldroidFragment {

	@Override
	public CaldroidSampleCustomAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		return new CaldroidSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData);
	}

}
