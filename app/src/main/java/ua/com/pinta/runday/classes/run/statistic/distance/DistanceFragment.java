package ua.com.pinta.runday.classes.run.statistic.distance;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ua.com.pinta.runday.R;


public class DistanceFragment extends Fragment {


    float max = 12.0f;

    public static DistanceFragment newInstance() {
        DistanceFragment fragment = new DistanceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public DistanceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("tag", "onCreateView - DistanceFragment");

        final boolean[] mMeasured = {false};

        final View v = inflater.inflate(R.layout.fragment_graph, container, false);


        final RelativeLayout root_layout = (RelativeLayout) v.findViewById(R.id.root_layout);
//
//        final TextView top_level_textView = (TextView) v.findViewById(R.id.top_statistic_level_textView);
//        final TextView center__level_textView = (TextView) v.findViewById(R.id.center_statistic_level_textView);
//        TextView bottom_level_textView = (TextView) v.findViewById(R.id.bottom_statistic_level_textView);

        final TextView first_day_tv = (TextView) v.findViewById(R.id.first_day_statistic_tv);
        final TextView second_day_tv = (TextView) v.findViewById(R.id.second_day_statistic_tv);
        final TextView third_day_tv = (TextView) v.findViewById(R.id.third_day_statistic_tv);
        final TextView forth_day_tv = (TextView) v.findViewById(R.id.forth_day_statistic_tv);
        final TextView fives_day_tv = (TextView) v.findViewById(R.id.fives_day_statistic_tv);
        final TextView sixs_day_tv = (TextView) v.findViewById(R.id.sixs_day_statistic_tv);
        final TextView sevens_day_tv = (TextView) v.findViewById(R.id.sevens_day_statistic_tv);

        final RelativeLayout first_day_column_layout = (RelativeLayout) v.findViewById(R.id.first_day_column_statistic_layout);
        final RelativeLayout second_day_column_layout = (RelativeLayout) v.findViewById(R.id.second_day_column_statistic_layout);
        final RelativeLayout third_day_column_layout = (RelativeLayout) v.findViewById(R.id.third_day_column_statistic_layout);
        final RelativeLayout forth_day_column_layout = (RelativeLayout) v.findViewById(R.id.forth_day_column_statistic_layout);
        final RelativeLayout fives_day_column_layout = (RelativeLayout) v.findViewById(R.id.fives_day_column_statistic_layout);
        final RelativeLayout sixs_day_column_layout = (RelativeLayout) v.findViewById(R.id.sixs_day_column_statistic_layout);
        final RelativeLayout sevens_day_column_layout = (RelativeLayout) v.findViewById(R.id.sevens_day_column_statistic_layout);


        root_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!mMeasured[0]) {

                    float kmInPx = (root_layout.getHeight() - 70) / max;

//                    top_level_textView.setText(max + " km");
//                    center__level_textView.setText(max / 2 + " km");


                    float first_day = 5.8f;
                    float second_day = 8.1f;
                    float third_day = 2.6f;
                    float forth_day = 6.8f;
                    float fives_day = 9.3f;
                    float sixs_day = 1.5f;
                    float sevens_day = 0.5f;

                    first_day_tv.setText(first_day +"km");
                    second_day_tv.setText(second_day +"km");
                    third_day_tv.setText(third_day +"km");
                    forth_day_tv.setText(forth_day +"km");
                    fives_day_tv.setText(fives_day +"km");
                    sixs_day_tv.setText(sixs_day +"km");
                    sevens_day_tv.setText(sevens_day +"km");


                    first_day_column_layout.setMinimumHeight((int) (kmInPx * first_day));
                    second_day_column_layout.setMinimumHeight((int) (kmInPx * second_day));
                    third_day_column_layout.setMinimumHeight((int) (kmInPx * third_day));
                    forth_day_column_layout.setMinimumHeight((int) (kmInPx * forth_day));
                    fives_day_column_layout.setMinimumHeight((int) (kmInPx * fives_day));
                    sixs_day_column_layout.setMinimumHeight((int) (kmInPx * sixs_day));
                    sevens_day_column_layout.setMinimumHeight((int) (kmInPx * sevens_day));


                    Log.d("tag", "" + root_layout.getHeight());
//                    top_level_textView.setText(root_layout.getHeight() + "");
                    mMeasured[0] = true;
                }


            }
        });
        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
