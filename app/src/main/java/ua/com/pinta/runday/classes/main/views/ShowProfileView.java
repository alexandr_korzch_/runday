package ua.com.pinta.runday.classes.main.views;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.classes.general.photo.Decoder;
import ua.com.pinta.runday.interfaces.listeners.IProfileListener;
import ua.com.pinta.runday.interfaces.views.IShow;

/**
 * Created by alexandr on 18.05.15.
 */
public class ShowProfileView implements IShow {

    MainActivity activity;

    ImageView profile_photo_imageView;
    Button download_photo_button;
    EditText profile_country_edit;
    EditText profile_city_edit;
    TextView profile_save_textView;
    EditText download_photo_edit;
    EditText profile_name_edit;
    EditText profile_age_edit;
    EditText profile_height_edit;
    EditText profile_weight_edit;
    CheckBox profile_man_checkBox;
    CheckBox profile_woman_checkBox;

    boolean firstLoading = true;

    public ShowProfileView(MainActivity activity) {
        if (activity.isLog()) Log.d("tag", "new ShowProfile");
        this.activity = activity;
    }

    @Override
    public void show() {
        if (activity.isLog()) Log.d("tag", "ShowProfileView  show()");
        activity.setContentView(R.layout.profile_layout);
        init();
    }

    private void init() {
        if (activity.isLog()) Log.d("tag", "init()");

        download_photo_edit = (EditText) activity.findViewById(R.id.download_photo_edit);
        profile_name_edit = (EditText) activity.findViewById(R.id.profile_name_edit);
        profile_age_edit = (EditText) activity.findViewById(R.id.profile_age_edit);
        profile_height_edit = (EditText) activity.findViewById(R.id.profile_height_edit);
        profile_weight_edit = (EditText) activity.findViewById(R.id.profile_weight_edit);

        profile_photo_imageView = (ImageView) activity.findViewById(R.id.profile_photo_imageView);

        profile_country_edit = (EditText) activity.findViewById(R.id.profile_country_edit);
        profile_city_edit = (EditText) activity.findViewById(R.id.profile_city_edit);

        profile_man_checkBox = (CheckBox) activity.findViewById(R.id.profile_man_checkBox);
        profile_woman_checkBox = (CheckBox) activity.findViewById(R.id.profile_woman_checkBox);

        User user = activity.getBeanContainer(false).getProfileDB(false).getProfileFromDB();


        profile_name_edit.setText(user.getFirstName());

        if (user.getAge() != 0) profile_age_edit.setText(user.getAge() + "");
        if (user.getHeight() != 0) profile_height_edit.setText(user.getHeight() + "");
        if (user.getWeight() != 0) profile_weight_edit.setText(user.getWeight() + "");

        profile_country_edit.setText(user.getCountry());
        profile_city_edit.setText(user.getCity());

        logOut("profile_man_checkBox - " + profile_man_checkBox);
        logOut("user - " + user);
        logOut("ShowProfileView user.getSex() - " + user.getSex());

        if (user.getSex() != null && user.getSex().equals("male")) {
            profile_man_checkBox.setChecked(true);
        } else {
            profile_man_checkBox.setChecked(false);
        }

        if (user.getSex() != null && user.getSex().equals("female")) {
            profile_woman_checkBox.setChecked(true);
        } else {
            profile_woman_checkBox.setChecked(false);
        }


        logOut(mUserProfile.bitmap + "");

/**/
        /*��������� ���� �� ��������, ���� ��� ���, �� �� ������, ��� ������ ��������� �� �������� ��� ������*/
        if (mUserProfile.photoInStorage != null && firstLoading) {
            Bitmap imageBitmap = null;
            try {
                Log.d("photo", "mUserProfile.photoInStorage - " + mUserProfile.photoInStorage);

                imageBitmap = new Decoder().decodeURI(mUserProfile.photoInStorage);
                profile_photo_imageView.setImageBitmap(imageBitmap);
                firstLoading = false;

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else if (mUserProfile.avatar != null && firstLoading) {
            ImageDownloader imageDownloader = new ImageDownloader(profile_photo_imageView);
            imageDownloader.execute(mUserProfile.avatar);
            firstLoading = false;

        } else {
            profile_photo_imageView.setImageBitmap(mUserProfile.bitmap);
        }


        /*��������� � ��� �����*/
        if (mUserProfile.name != null && !mUserProfile.name.equals("null")) {
            profile_name_edit.setText(mUserProfile.name);
        }


        IProfileListener profilListener = activity.getBeanContainer(false).getProfileListener(true);

        download_photo_button = (Button) activity.findViewById(R.id.download_photo_button);
        download_photo_button.setOnClickListener(profilListener);

        profile_save_textView = (TextView) activity.findViewById(R.id.profile_save_textView);
        profile_save_textView.setOnClickListener(profilListener);


    }

    class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public ImageDownloader(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private void logOut(String message) {
        Log.d("tag", message);
        //mapActivity.map_textView.append(message + "\n");
    }

}
