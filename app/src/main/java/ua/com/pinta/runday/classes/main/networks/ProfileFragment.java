package ua.com.pinta.runday.classes.main.networks;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.listener.OnRequestSocialPersonCompleteListener;
import com.github.gorbin.asne.core.persons.SocialPerson;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import java.io.InputStream;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;


public class ProfileFragment extends Fragment implements OnRequestSocialPersonCompleteListener {


    private static final String NETWORK_ID = "NETWORK_ID";
    private SocialNetwork socialNetwork;
    private int networkId;
    private ImageView photo;
    private TextView name;
    private TextView id;
    private TextView info;
    private Button ok_bt;
    private RelativeLayout frame;
    private SocialPerson socialPerson;

    public static ProfileFragment newInstannce(int id) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(NETWORK_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        networkId = getArguments().containsKey(NETWORK_ID) ? getArguments().getInt(NETWORK_ID) : 0;
        //((NetworksActivity)getActivity()).getSupportActionBar().setTitle("Profile");

        View rootView = inflater.inflate(R.layout.profile_fragment, container, false);

        frame = (RelativeLayout) rootView.findViewById(R.id.frame);
        photo = (ImageView) rootView.findViewById(R.id.imageView);
        name = (TextView) rootView.findViewById(R.id.name);
        id = (TextView) rootView.findViewById(R.id.id);
        info = (TextView) rootView.findViewById(R.id.info);

        ok_bt = (Button) rootView.findViewById(R.id.ok_bt);
        ok_bt.setOnClickListener(OkClick);

        colorProfile(networkId);

        socialNetwork = SocialNetworksRegFragment.mSocialNetworkManager.getSocialNetwork(networkId);
        socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
        socialNetwork.requestCurrentPerson();

        //NetworksActivity.showProgress("Loading social person");

        return rootView;
    }


    @Override
    public void onRequestSocialPersonSuccess(int i, SocialPerson socialPerson) {
        this.socialPerson = socialPerson;
        //NetworksActivity.hideProgress();
        name.setText(socialPerson.name);
        id.setText(socialPerson.id);
        ImageDownloader imageDownloader = new ImageDownloader(photo);
        imageDownloader.execute(socialPerson.avatarURL);
        //NetworksActivity.hideProgress();
    }



    class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public ImageDownloader(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        //NetworksActivity.hideProgress();
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }


    private View.OnClickListener OkClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            //��������� ����� ��������, ����������� �� ������� ��������

            mUserProfile.name = "";
            mUserProfile.url = "";
            mUserProfile.email = "";
            mUserProfile.avatar = "";

            mUserProfile.name = socialPerson.name;
            mUserProfile.url = socialPerson.profileURL;
            mUserProfile.email = socialPerson.email;
            mUserProfile.avatar = socialPerson.avatarURL;

            startMainActivity();
        }
    };

    private void startMainActivity(){

        Intent intent = new Intent(getActivity(), MainActivity.class);//����� ��������� � ����� �������� ����������
        getActivity(). setResult(getActivity().RESULT_OK, intent);
        getActivity().finish();

    }


    private void colorProfile(int networkId){
        int color = getResources().getColor(R.color.dark);
        int image = R.drawable.user;
        switch (networkId) {
            case TwitterSocialNetwork.ID:
                color = getResources().getColor(R.color.twitter);
                image = R.drawable.user;
                break;
            case FacebookSocialNetwork.ID:
                color = getResources().getColor(R.color.facebook);
                image = R.drawable.user;
                break;
            case VkSocialNetwork.ID:
                color = getResources().getColor(R.color.vk);
                image = R.drawable.user;
                break;

        }
        frame.setBackgroundColor(color);
        name.setTextColor(color);
        ok_bt.setBackgroundColor(color);
        photo.setBackgroundColor(color);
        photo.setImageResource(image);
    }

    private void logOut(String message) {
        Log.d("tag", message);
    }

}
