package ua.com.pinta.runday.classes.main.listeners;

import android.util.Log;
import android.view.View;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.checkers.IServiceChecker;
import ua.com.pinta.runday.interfaces.listeners.IWelcomeListener;
import ua.com.pinta.runday.interfaces.checkers.IServiceReceiver;


public class WelcomeListener implements IWelcomeListener {

    private MainActivity activity;

    private IServiceChecker serviceChecker;
    private IServiceReceiver serviceController;

    public WelcomeListener(MainActivity activity) {
        this.activity = activity;
        init();
        if(activity.isLog())Log.d("tag", "new WelcomeListener()");
    }

    private void init(){

        if(activity.isLog())Log.d("tag", "init()");

        serviceChecker = activity.getBeanContainer(false).getServiceChecker(false);
        serviceController = activity.getBeanContainer(false).getServiceController(false);

    }


    @Override
    public void onClick(View v) {

        if(activity.isLog())Log.d("tag", "WelcomeListener onClick()");

        boolean enableButtons = true;

        if(!serviceChecker.isOnline(activity)){
            enableButtons = false;
            serviceController.connectInternet();

        }
        if(!serviceChecker.isGooglePlayServices(activity)){
            enableButtons = false;
            serviceController.receiveGooglePlayServices();
        }
        if(!serviceChecker.isGPS(activity)){
            enableButtons = false;
            serviceController.turnOnGPS();
        }

        if(enableButtons) {

            switch (v.getId()) {

                case R.id.enter_bt: {
                    if(activity.isLog())Log.d("tag", "enter_bt");
                    activity.getBeanContainer(false).getViewRouter(false).showEnter();
                    break;
                }

                case R.id.reg_bt: {
                    if(activity.isLog())Log.d("tag", "reg_bt");
                    activity.getBeanContainer(false).getViewRouter(false).showRegistration();
                    break;
                }

            }
        }

    }

}
