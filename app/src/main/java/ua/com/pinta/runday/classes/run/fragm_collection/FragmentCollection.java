package ua.com.pinta.runday.classes.run.fragm_collection;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.RunActivity;
import ua.com.pinta.runday.classes.run.proffile.ProfileFragment;
import ua.com.pinta.runday.classes.run.actions.ActionsFragment;
import ua.com.pinta.runday.classes.run.callories.CalloriesFragment;
import ua.com.pinta.runday.classes.run.friends.FriendsFragment;
import ua.com.pinta.runday.classes.run.map.MapFragment;
import ua.com.pinta.runday.classes.run.music.MusicFragment;
import ua.com.pinta.runday.classes.run.sittings.SittingsFragment;
import ua.com.pinta.runday.classes.run.statistic.StatisticFragment;
import ua.com.pinta.runday.classes.run.targets.TargetsFragment;
import ua.com.pinta.runday.classes.run.timetable.TimeTableFragment;

/**
 * Created by Cach on 04.06.2015.
 */
public class FragmentCollection {

    RunActivity activity;

    private List<CharSequence> titles = new ArrayList<CharSequence>();
    private List<Fragment> fragments = new ArrayList<Fragment>();

    private final MapFragment mapFragment;
    private final MusicFragment musicFragment;
    private final FriendsFragment friendsFragment;
    private final TimeTableFragment timeTableFragment;

    private Fragment lastfragment;

    public Fragment getLastfragment() {
        return lastfragment;
    }

    public FragmentCollection(RunActivity activity) {
        this.activity = activity;

        mapFragment = new MapFragment();
        musicFragment = new MusicFragment();
        friendsFragment = new FriendsFragment();
        timeTableFragment = new TimeTableFragment();

        fragments.add(mapFragment);
        fragments.add(musicFragment);
        fragments.add(friendsFragment);
        fragments.add(timeTableFragment);

        titles.add(activity.getResources().getString(R.string.run));
        titles.add(activity.getResources().getString(R.string.music));
        titles.add(activity.getResources().getString(R.string.friends));
        titles.add(activity.getResources().getString(R.string.time_table));

    }

    public List<Fragment> getFragments() {
        return fragments;
    }

    public List<CharSequence> getTitles() {
        return titles;
    }

    public int getFragmentCount() {
        return fragments.size();
    }

    public void addProfileFragment() {

        lastfragment = ProfileFragment.newInstance();
        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_profile));

    }

    public void addTargetsFragment() {

        lastfragment = TargetsFragment.newInstance();
        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_targets));

    }

    public void addActionsFragment() {

        lastfragment = ActionsFragment.newInstance();
        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_activity));

    }

    public void addStatisticFragment() {

        lastfragment = StatisticFragment.newInstance();
        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_statistic));

    }

    public void addCalloriesFragment() {

        lastfragment = CalloriesFragment.newInstance();

        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_callories));

    }

    public void addSittingsFragment() {

        lastfragment = SittingsFragment.newInstance();
        addSelectFragment(
                lastfragment,
                activity.getString(R.string.m_sittings));
    }


    private void addSelectFragment(Fragment fragment, String title) {

        removeLast();
        fragments.add(fragment);
        titles.add(title);
        activity.initAdapter(4);
    }


    public void removeLast() {
        if (fragments.size() > 4) {
            fragments.remove(4);
            titles.remove(4);
        }
    }
}
