package ua.com.pinta.runday.classes.run.actions;


import java.util.Date;

/**
 * Created by alexandr on 07.06.15.
 */
public class ScamperModel {

    private Date date;
    private float speed;
    private long time;
    private int distanse;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getDistanse() {
        return distanse;
    }

    public void setDistanse(int distanse) {
        this.distanse = distanse;
    }
}
