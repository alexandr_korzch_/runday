package ua.com.pinta.runday.classes.main.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.classes.general.dao.DBHelper;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.interfaces.dao.IProfileDB;


/**
 * Created by alexandr on 17.05.15.
 */
public class ProfileDB implements IProfileDB {

    private MainActivity activity;
    private DBHelper dbHelper;
    private SQLiteDatabase database;
    private Cursor cursor;

    public ProfileDB(MainActivity activity, DBHelper dbHelper) {
        if(activity.isLog()) Log.d("tag", "new ProfileDB()");
        this.activity = activity;
        this.dbHelper = dbHelper;
    }

    @Override
    public void updateProfileInDB(User user) {
        Log.d("tag", "updateProfileInDB(User user)");
        Log.d("tag", "user.getSex()to set in DB - " + user.getSex());

        openDB();

        ContentValues row = new ContentValues();

        row.put("firstname", user.getFirstName());
        row.put("sex", user.getSex());
        row.put("age", user.getAge());
        row.put("height", user.getHeight());
        row.put("weight", user.getWeight());
        row.put("country", user.getCountry());
        row.put("city", user.getCity());
        row.put("foto", user.getFoto());

        String email = new LoginDB(dbHelper).getUserEmail();



        database.update("user", row, "email="+"'"+email+"'", null);
//
//        int updCount = database.update("user", row, "email = ?",new String[] { email });

//        Log.d("tag","updCount - " + updCount );

//        database.insert("user", null, row);

        closeDB();

    }

    @Override
    public User getProfileFromDB(){

        if(activity.isLog()) Log.d("tag", "getProfileFromDB()");

        User user = activity.getBeanContainer(false).getUser(false);

        openDB();

        cursor = database.rawQuery("SELECT * FROM user", null);
        cursor.moveToFirst();

        try {
            user.setFirstName(cursor.getString(cursor.getColumnIndex("firstname")));
            user.setAge(cursor.getInt(cursor.getColumnIndex("age")));
            user.setHeight(cursor.getInt(cursor.getColumnIndex("height")));
            user.setWeight(cursor.getInt(cursor.getColumnIndex("weight")));
            user.setCountry(cursor.getString(cursor.getColumnIndex("country")));
            user.setCity(cursor.getString(cursor.getColumnIndex("city")));
            user.setFoto(cursor.getString(cursor.getColumnIndex("foto")));
            user.setSex(cursor.getString(cursor.getColumnIndex("sex")));


            Log.d("tag", "get sex - "+user.getSex());

        }catch(CursorIndexOutOfBoundsException ex){

            Log.d("tag", "CursorIndexOutOfBoundsException ex " + ex);

        }

        closeDB();
        return user;
    }


    private void openDB() throws SQLException {
        if(activity.isLog()) Log.d("tag", "openDB()");
        try {
            database = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void closeDB() {
        if(activity.isLog()) Log.d("tag", "closeDB()");
        try {
            if (database != null && database.isOpen())
                dbHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
