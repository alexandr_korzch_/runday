package ua.com.pinta.runday.classes.run.map;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import ua.com.pinta.runday.R;
import ua.com.pinta.runday.RunActivity;
import ua.com.pinta.runday.classes.run.map.models.Track;
import ua.com.pinta.runday.classes.run.map.service.MapService;
import ua.com.pinta.runday.classes.run.tabs.SlidingTabLayout;
import ua.com.pinta.runday.interfaces.models.ITrack;
//import ua.com.pinta.runday.classes.general.dao.TrackDB;
//import ua.com.pinta.runday.classes.run.map.RouteHandler;


public class MapFragment extends Fragment {

    private MapView mMapView;
    public GoogleMap mMap;
    public mLocation mLocation;
    public DrawOnMap drawOnMap;
    public ITrack track;
    private RunActivity activity;
    public MapService mapService;

    //    public TrackDB trackDB;
    public MapListener mapListener;

    public boolean recordTrack = false;
//    public RouteHandler routHandler;

    public Button run__map_button;

//    TextView textView;

    public SlidingTabLayout tabsL;


    public void setTabs(SlidingTabLayout tabs) {
        this.tabsL = tabs;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (RunActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container,
                false);



        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMap = mMapView.getMap();

        if (mMap != null) {

            new Handler().postDelayed(new Runnable() {

                public void run() {
                    Log.d("tag", "run()");
                    mMap.setMyLocationEnabled(true);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    mMap.getUiSettings().setCompassEnabled(true);

                    initObjects();
                    loadingLocation();
                }

            }, 300);
        } else logOut("map = null");

        run__map_button = (Button) v.findViewById(R.id.run__map_button);
        run__map_button.setOnClickListener(new MapListener(this));
        run__map_button.setEnabled(false);

//        textView = (TextView)v.findViewById(R.id.map_textView);

        return v;
    }


    void initObjects() {
        mLocation = new mLocation(this);
        mapService = new MapService(this);
//        trackDB = new TrackDB(this.getActivity());
        track = new Track();
        drawOnMap = new DrawOnMap(this);
//        routHandler = new RouteHandler(this);
    }


    private void loadingLocation() {
        Log.d("tag", "loadingLocation()");
        mLocation.settingMyLocation();
    }


    public void animCamera(double lat, double lon, int zoom, int bearing, int tilt) {
        logOut("animCamera");
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lon))
                .zoom(zoom)
                .bearing(bearing)
                .tilt(tilt)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void logOut(String message) {
        Log.d("tag", message);
    }

}
