package ua.com.pinta.runday.classes.run.navigationdrawer;

import android.util.Log;
import android.view.View;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.RunActivity;

/**
 * Created by Cach on 04.06.2015.
 */
public class NavigationListener implements View.OnClickListener{


    RunActivity activity;

    public NavigationListener() {

    }

    public void setActivity(RunActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.navig_profile:{
                logOut("activity.fragmentCollection - " + activity.fragmentCollection);
                activity.fragmentCollection.addProfileFragment();

                break;
            }

            case R.id.navig_targets:{
                activity.fragmentCollection.addTargetsFragment();
                        logOut("navig_targets");
                break;
            }

            case R.id.navig_actions:{
                activity.fragmentCollection.addActionsFragment();
                logOut("navig_actions");
                break;
            }

            case R.id.navig_statistic:{
                activity.fragmentCollection.addStatisticFragment();
                logOut("navig_statistic");
                break;
            }

            case R.id.navig_calories:{
                activity.fragmentCollection.addCalloriesFragment();
                logOut("navig_calories");
                break;
            }

            case R.id.navig_sittings:{
                activity.fragmentCollection.addSittingsFragment();
                        logOut("navig_sittings");
                break;
            }


        }

    }



    private void logOut(String message){
        Log.d("tag", message);
    }

}
