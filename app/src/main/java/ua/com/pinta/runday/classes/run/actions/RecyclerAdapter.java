package ua.com.pinta.runday.classes.run.actions;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ua.com.pinta.runday.R;

/**
 * Created by alexandr on 07.06.15.
 */
public class RecyclerAdapter  extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List <ScamperModel>itemsData;

    public RecyclerAdapter(List itemsData) {
        this.itemsData = itemsData;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.actions_card_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.date_textV.setText(itemsData.get(position).getDate().toString());

        viewHolder.actions_speed_textV.setText(itemsData.get(position).getSpeed()+"");
        viewHolder.actions_time_textV.setText(itemsData.get(position).getTime()+"");
        viewHolder.actions_distance_textV.setText(itemsData.get(position).getDistanse()+"");

//        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date_textV;

        public TextView actions_speed_textV;
        public TextView actions_time_textV;
        public TextView actions_distance_textV;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            date_textV = (TextView) itemLayoutView.findViewById(R.id.date_textV);

            actions_speed_textV = (TextView) itemLayoutView.findViewById(R.id.actions_speed_textV);
            actions_time_textV = (TextView) itemLayoutView.findViewById(R.id.actions_time_textV);
            actions_distance_textV = (TextView) itemLayoutView.findViewById(R.id.actions_distance_textV);

//            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}