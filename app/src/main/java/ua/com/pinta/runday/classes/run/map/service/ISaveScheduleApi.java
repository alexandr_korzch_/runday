package ua.com.pinta.runday.classes.run.map.service;

import java.util.Date;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Cach on 05.06.2015.
 */
public interface ISaveScheduleApi {


    @POST("/api/v1/save_schedule")
    void saveSchedule(
            @Query("token") String token,
            @Query("firstname") String firstname,
            @Query("date") Date date,
            @Query("distance") int distance,
            @Query("type") String type,
            @Query("training_id") int training_id,
            @Query("route_id") int route_id,
            @Query("status") String status,
            Callback<Response> response);

}
