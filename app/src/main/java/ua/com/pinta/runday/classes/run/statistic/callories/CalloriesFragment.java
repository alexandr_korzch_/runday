package ua.com.pinta.runday.classes.run.statistic.callories;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ua.com.pinta.runday.R;


public class CalloriesFragment extends Fragment {


    float max = 600.0f;


    public static CalloriesFragment newInstance() {
        CalloriesFragment fragment = new CalloriesFragment();
        return fragment;
    }

    public CalloriesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("tag", "onCreateView - CalloriesFragment");

        final boolean[] mMeasured = {false};

        final View v = inflater.inflate(R.layout.fragment_graph, container, false);


        final RelativeLayout root_layout = (RelativeLayout) v.findViewById(R.id.root_layout);

//        final TextView top_level_textView = (TextView) v.findViewById(R.id.top_statistic_level_textView);
//        final TextView center__level_textView = (TextView) v.findViewById(R.id.center_statistic_level_textView);
//        TextView bottom_level_textView = (TextView) v.findViewById(R.id.bottom_statistic_level_textView);

        final TextView first_day_tv = (TextView) v.findViewById(R.id.first_day_statistic_tv);
        final TextView second_day_tv = (TextView) v.findViewById(R.id.second_day_statistic_tv);
        final TextView third_day_tv = (TextView) v.findViewById(R.id.third_day_statistic_tv);
        final TextView forth_day_tv = (TextView) v.findViewById(R.id.forth_day_statistic_tv);
        final TextView fives_day_tv = (TextView) v.findViewById(R.id.fives_day_statistic_tv);
        final TextView sixs_day_tv = (TextView) v.findViewById(R.id.sixs_day_statistic_tv);
        final TextView sevens_day_tv = (TextView) v.findViewById(R.id.sevens_day_statistic_tv);

        final RelativeLayout first_day_column_layout = (RelativeLayout) v.findViewById(R.id.first_day_column_statistic_layout);
        final RelativeLayout second_day_column_layout = (RelativeLayout) v.findViewById(R.id.second_day_column_statistic_layout);
        final RelativeLayout third_day_column_layout = (RelativeLayout) v.findViewById(R.id.third_day_column_statistic_layout);
        final RelativeLayout forth_day_column_layout = (RelativeLayout) v.findViewById(R.id.forth_day_column_statistic_layout);
        final RelativeLayout fives_day_column_layout = (RelativeLayout) v.findViewById(R.id.fives_day_column_statistic_layout);
        final RelativeLayout sixs_day_column_layout = (RelativeLayout) v.findViewById(R.id.sixs_day_column_statistic_layout);
        final RelativeLayout sevens_day_column_layout = (RelativeLayout) v.findViewById(R.id.sevens_day_column_statistic_layout);


        root_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!mMeasured[0]) {

                    float kklInPx = (root_layout.getHeight() - 70) / max;

//                    top_level_textView.setText(max +"kkl");
//                    center__level_textView.setText(max / 2 +"kkl");


                    int first_day = 245;
                    int second_day = 330;
                    int third_day = 550;
                    int forth_day = 680;
                    int fives_day = 250;
                    int sixs_day = 340;
                    int sevens_day = 150;

                    first_day_tv.setText(first_day +"kkl");
                    second_day_tv.setText(second_day +"kkl");
                    third_day_tv.setText(third_day +"kkl");
                    forth_day_tv.setText(forth_day +"kkl");
                    fives_day_tv.setText(fives_day +"kkl");
                    sixs_day_tv.setText(sixs_day +"kkl");
                    sevens_day_tv.setText(sevens_day +"kkl");


                    first_day_column_layout.setMinimumHeight((int) (kklInPx * first_day));
                    second_day_column_layout.setMinimumHeight((int) (kklInPx * second_day));
                    third_day_column_layout.setMinimumHeight((int) (kklInPx * third_day));
                    forth_day_column_layout.setMinimumHeight((int) (kklInPx * forth_day));
                    fives_day_column_layout.setMinimumHeight((int) (kklInPx * fives_day));
                    sixs_day_column_layout.setMinimumHeight((int) (kklInPx * sixs_day));
                    sevens_day_column_layout.setMinimumHeight((int) (kklInPx * sevens_day));


                    Log.d("tag", "" + root_layout.getHeight());
//                    top_level_textView.setText(root_layout.getHeight() + "");
                    mMeasured[0] = true;
                }


            }
        });

        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
