package ua.com.pinta.runday.classes.run.timetable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.Calendar;
import java.util.Date;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.RunActivity;
import ua.com.pinta.runday.classes.run.tabs.SlidingTabLayout;
import ua.com.pinta.runday.classes.run.timetable.caldroid.general.CaldroidSampleCustomFragment;
import ua.com.pinta.runday.classes.run.timetable.caldroid.library.fragment.CaldroidFragment;
import ua.com.pinta.runday.classes.run.timetable.caldroid.library.fragment.CaldroidListener;
import ua.com.pinta.runday.classes.run.timetable.dialog.DFragment;


public class TimeTableFragment extends Fragment {

    public RunActivity activity;
    public SlidingTabLayout tabsL;
    private RelativeLayout root_layout_taime_table;

    private CaldroidFragment caldroidFragment;

    public static TimeTableFragment newInstance() {
        TimeTableFragment fragment = new TimeTableFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public TimeTableFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (RunActivity) getActivity();
        tabsL = activity.tabsL;

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_time_table, container, false);

        root_layout_taime_table = (RelativeLayout) view.findViewById(R.id.root_layout_taime_table);

//        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        caldroidFragment = new CaldroidSampleCustomFragment();

        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState, "CALDROID_SAVED_STATE");
        } else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

            args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidDefaultDark);

            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();

        FragmentTransaction t = getChildFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();


        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                showPopup(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
            }

            @Override
            public void onLongClickDate(Date date, View view) {
            }

            @Override
            public void onCaldroidViewCreated() {
            }

        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);


        return view;
    }


    private void setCustomResourceForDates() {
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date date = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();

        if (caldroidFragment != null) {

            caldroidFragment.setBackgroundResourceForDate(R.drawable.select_date_fon, date);
            caldroidFragment.setTextColorForDate(R.color.text_color_in_select_date, date);

            caldroidFragment.setBackgroundResourceForDate(R.drawable.select_date_fon, greenDate);
            caldroidFragment.setTextColorForDate(R.color.text_color_in_select_date, greenDate);

        }
    }


    public void showPopup(Date date) {

        FragmentManager fm = activity.getSupportFragmentManager();
        DFragment dFragment = new DFragment();
        dFragment.setDate(date);
        dFragment.show(fm, "Dialog Fragment");

    }


    private void logOut(String message) {
        Log.d("tag", message);
        //mapActivity.map_textView.append(message + "\n");
    }

}
