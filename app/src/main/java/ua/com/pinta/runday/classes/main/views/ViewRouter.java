package ua.com.pinta.runday.classes.main.views;

import android.util.Log;

import java.util.ArrayList;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.interfaces.views.IViewRouter;

/**
 * Created by alexandr on 16.05.15.
 */
public class ViewRouter implements IViewRouter {

    private MainActivity activity;
    private int indexView = 0;
    private ArrayList<String> views = new ArrayList<String>();

    public ViewRouter(MainActivity activity) {
        this.activity = activity;
        if(activity.isLog()) Log.d("tag", "new ViewRouter");
    }

    @Override
    public void showWelcome() {
        if(activity.isLog()) Log.d("tag", "showWelcome()");
        activity.getBeanContainer(false).getShowView(false, "welcome").show();
        saveStory("welcome");
    }

    @Override
    public void showEnter() {
        if(activity.isLog()) Log.d("tag", "showEnter()");
        activity.getBeanContainer(false).getShowView(false, "enter").show();
        saveStory("enter");
    }

    @Override
    public void showRegistration() {
        if(activity.isLog()) Log.d("tag", "showRegistration()");
        activity.getBeanContainer(false).getShowView(false, "registration").show();
        saveStory("registration");
    }

        @Override
    public void showProfile() {
        if(activity.isLog()) Log.d("tag", "Router showProfile()");
        activity.getBeanContainer(false).getShowView(false, "profile").show();
        saveStory("profile");

    }

//    @Override
//    public void showPager() {
//        if(activity.isLog()) Log.d("tag", "showPager()");
//        activity.getBeanContainer(false).getShowView(false, "pager").show();
//        saveStory("pager");
//
//    }


    private void saveStory(String viewName){
        if(activity.isLog()) Log.d("tag", "saveStory("+viewName+")");
        views.add(viewName);
        indexView = views.size()-1;
    }

    public void showPreviousView(){
        if(activity.isLog()) Log.d("tag", "showPreviousView()");
        try {
            activity.getBeanContainer(false).getShowView(false, views.get(--indexView)).show();
            saveStory(views.get(indexView));
        }catch(ArrayIndexOutOfBoundsException ex){}



    }


}
