package ua.com.pinta.runday.classes.main.servises;

import android.os.Environment;
import android.os.Message;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.general.constatnts.Variables;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.interfaces.servises.IProfileSaveService;


/**
 * Created by alexandr on 18.05.15.
 */
public class ProfileService implements IProfileSaveService {

    MainActivity activity;

    User user;

    private String dir;
    private String fileName;

    private String server;

    public ProfileService(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void saveProfileFromActivity(User user) {
        Log.d("tag", "saveProfileFromActivity()");


        this.user = user;
        dir = Environment.getExternalStorageDirectory().toString() + activity.getString(R.string.foto_dir);
        fileName = activity.getString(R.string.portret_file_name);
        server = activity.getString(R.string.server);

        makeRequest();

    }

    private void makeRequest() {

        Log.d("tag", "makeRequest()");


        File file = new File(dir + "/" +fileName);
        TypedFile tFile = new TypedFile("avatar", file);

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(server)
                .build();
        ProfileApi api = adapter.create(ProfileApi.class);
        api.sendProfile(
                Variables.token,
                user.getFirstName(),
                user.getSex(),
                user.getAge()+"",
                user.getHeight()+"",
                user.getWeight()+"",
                user.getCountry(),
                user.getCity(),
                tFile,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        Log.d("tag", "response - " + response);

                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    Log.d("tag", "line - " + line);

                                    sb.append(line);
                                }

                                Log.d("tag", "response - " + sb.toString());

                                parsJSON(sb.toString());


                            } catch (IOException e) {
                                Log.d("tag", "IOException - " + e);

                            }
                        } catch (IOException e) {
                            Log.d("tag", "IOException2 - " + e);

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("tag", "error - " + error);
                    }
                });


    }


    protected void parsJSON(String result) {

        if (activity.isLog()) Log.d("tag", "ParsJSON(String content)");
        if (activity.isLog()) Log.d("tag", "result - " + result);

        try {

            JSONObject response = new JSONObject(result);

            if (response.getString("status").equalsIgnoreCase("success")) {

                showSaveProfile(true);
                showMap();

            } else {
                if (activity.isLog()) Log.d("tag", "!result");
            }

        } catch (JSONException e1) {
            if (activity.isLog()) Log.d("tag", "" + e1);
            showSaveProfile(false);
        } catch (NullPointerException en) {
            if (activity.isLog()) Log.d("tag", "" + "" + en);
            showSaveProfile(false);
        }

    }


    private void showSaveProfile(boolean success) {
        Message msg;
        if (success) {
            msg = activity.getHandler().obtainMessage(1, "run������� ��������");
            activity.getHandler().sendMessage(msg);
        } else {
            msg = activity.getHandler().obtainMessage(1, "������� ��������� �� �������, \n���������� ��������� �������");
            activity.getHandler().sendMessage(msg);
        }
    }

    private void showMap() {
        Message msg = activity.getHandler().obtainMessage(2, "map");
        activity.getHandler().sendMessage(msg);
    }
}
