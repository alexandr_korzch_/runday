package ua.com.pinta.runday.classes.run.map.models;

import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import ua.com.pinta.runday.interfaces.models.ITrack;

/**
 * Created by Cach on 20.05.2015.
 */
public class Track implements ITrack {

    private List<PointOnTrack> track = new ArrayList<PointOnTrack>();

    private float averageSpeed;
    private float distanceInMeter;
    private long timeInSeconds;

    public Track() {
    }

    @Override
    public List<PointOnTrack> getTrack() {
        return track;
    }

    @Override
    public void addPoint(
            Location startLoc,
            Location curLoc,
            float distanceM,
            long timeInSec,
            float speedMinSec
    ) {
        logOut("addPoint()");

        PointOnTrack pointOnTrack = new PointOnTrack();

        pointOnTrack.setStart(startLoc);
        pointOnTrack.setEnd(curLoc);

        pointOnTrack.setDistance(distanceM);
        logOut("distance - " + distanceM);

        pointOnTrack.setTimeInSec(timeInSec);
        logOut("timeInSec -" + timeInSec);

        pointOnTrack.setSpeed(speedMinSec);
        logOut("speedMinSec - " + speedMinSec);

        track.add(pointOnTrack);
        logOut("track.size() - "+track.size());

        logOut("\n");

    }



    @Override
    public void recalculation() {
        logOut("recalculation()");
        logOut("track.size() - "+track.size());

        averageSpeed = 0.0f;
        distanceInMeter = 0.0f;
        timeInSeconds = 0l;

        for(PointOnTrack point: track){
            logOut("track.get");
            distanceInMeter += point.getDistance();
            timeInSeconds += point.getTimeInSec();
        }
        averageSpeed = (distanceInMeter)/ (timeInSeconds);
        logOut("distanceInMeter - " + distanceInMeter);
        logOut("timeInSeconds - "+timeInSeconds);
        logOut("averageSpeed - " + averageSpeed);
        logOut("\n");
    }



    @Override
    public float getDistanceInMeters() {
        logOut("getDistanceInMeters()");
        logOut("distanceInMeter - "+distanceInMeter);
        logOut("\n");
        return distanceInMeter;
    }

    @Override
    public long getTimeInSeconds() {
        logOut("getTimeInSeconds()");
        logOut("timeInSeconds - "+timeInSeconds);
        logOut("\n");
        return timeInSeconds;
    }

    @Override
    public float getAverageSpeed() {
        logOut("getAverageSpeed()");
        logOut("averageSpeed - "+averageSpeed);
        logOut("\n");
        if(averageSpeed == Float.NaN)return 0.0f;
        return averageSpeed;
    }

    private void logOut(String message){
        Log.d("tag", message);
    }
}
