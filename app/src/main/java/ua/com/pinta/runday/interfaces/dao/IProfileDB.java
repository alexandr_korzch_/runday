package ua.com.pinta.runday.interfaces.dao;


import ua.com.pinta.runday.classes.main.models.User;

/**
 * Created by alexandr on 17.05.15.
 */
public interface IProfileDB {

    void updateProfileInDB(User user);

    User getProfileFromDB();

}
