package ua.com.pinta.runday.classes.run.map;

import android.util.Log;
import android.view.View;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.listeners.IMapListener;

/**
 * Created by Cach on 20.05.2015.
 */
public class MapListener implements IMapListener {

    MapFragment mapFragment;

    public MapListener(MapFragment mapFragment) {
        this.mapFragment = mapFragment;
    }

    @Override
    public void onClick(View v) {
        logOut("onClick()");

        if(!mapFragment.recordTrack){
            mapFragment.track.getTrack().clear();
            mapFragment.recordTrack = true;
            mapFragment.mMap.clear();
            mapFragment.drawOnMap.addMarkerToMap(
                    mapFragment.mLocation.getMyLocation().getLatitude(),
                    mapFragment.mLocation.getMyLocation().getLongitude());

            mapFragment.run__map_button.setBackground(
                    mapFragment.getResources()
                            .getDrawable(R.drawable.zavershit));
        }else{
            mapFragment.track.recalculation();
            mapFragment.drawOnMap.showPopup();

            mapFragment.recordTrack = false;
            mapFragment.drawOnMap.addMarkerToMap(
                    mapFragment.mLocation.getMyLocation().getLatitude(),
                    mapFragment.mLocation.getMyLocation().getLongitude());
            mapFragment.run__map_button.setBackground(
                    mapFragment.getResources()
                            .getDrawable(R.drawable.start_run));
        }
    }
    private void logOut(String message){
        Log.d("tag", message);
    }
}
