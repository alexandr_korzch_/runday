package ua.com.pinta.runday.classes.main.networks;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.vk.sdk.VKScope;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import ua.com.pinta.runday.NetworksActivity;
import ua.com.pinta.runday.R;


public class SocialNetworksRegFragment extends Fragment implements
        SocialNetworkManager.OnInitializationCompleteListener,
        OnLoginCompleteListener {

    public static SocialNetworkManager mSocialNetworkManager;

    // scopes
    ArrayList<String> facebookScope;
    String[] vkontakteScope;


    //keys
    String TWITTER_CONSUMER_KEY;
    String TWITTER_CONSUMER_SECRET;
    String TWITTER_CALLBACK_URL;
    String VK_KEY;


    private int netId;
    int networkId;


    public SocialNetworksRegFragment() {
        logOut("new SocialNetworksRegFragment()");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_network, container, false);
//        ((NetworksActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);

        logOut("onCreateView()");
        initKeys();
        initScopes();
        initNetworkManager();
        return rootView;
    }



    private void initKeys(){
        logOut("initKeys()");

        TWITTER_CONSUMER_KEY = getActivity().getString(R.string.twitter_consumer_key);
        TWITTER_CONSUMER_SECRET = this.getActivity().getString(R.string.twitter_consumer_secret);
        TWITTER_CALLBACK_URL = this.getActivity().getString(R.string.twitter_callback_url);
        VK_KEY = getActivity().getString(R.string.vk_key);

    }



    private void initScopes() {
        logOut("initScopes()");

        facebookScope = new ArrayList<String>();
        facebookScope.addAll(Arrays.asList("public_profile, email"));

        vkontakteScope = new String[] {
                VKScope.WALL,
                VKScope.ADS,
                VKScope.AUDIO,
                VKScope.DIRECT,
                VKScope.DOCS,
                VKScope.FRIENDS,
                VKScope.MESSAGES,
                VKScope.NOHTTPS,
                VKScope.NOTIFICATIONS,
                VKScope.OFFLINE,
                VKScope.PAGES,
                VKScope.PHOTOS,
                VKScope.STATS,
                VKScope.STATUS,
                VKScope.VIDEO,
                VKScope.NOHTTPS,

        };

    }

    private void initNetworkManager() {
        logOut("initNetworkManager()");
        mSocialNetworkManager = (SocialNetworkManager)getFragmentManager()
                .findFragmentByTag(NetworksActivity.SOCIAL_NETWORK_TAG);

        if (mSocialNetworkManager == null){
            mSocialNetworkManager = new SocialNetworkManager();
        } else if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
            List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
            for (SocialNetwork socialNetwork : socialNetworks) {
                socialNetwork.setOnLoginCompleteListener(this);
                logOut("socialNetwork");
            }
        }
        logOut("mSocialNetworkManager - " + mSocialNetworkManager);
        loginInNetWork(netId);
    }

    public void loginInNetWork(int netId){
        logOut("loginInNetWork()");

        networkId = 0;

            if(netId == TwitterSocialNetwork.ID){
                TwitterSocialNetwork twitterNetwork = new TwitterSocialNetwork(
                        this,
                        TWITTER_CONSUMER_KEY,
                        TWITTER_CONSUMER_SECRET,
                        TWITTER_CALLBACK_URL);
                mSocialNetworkManager.addSocialNetwork(twitterNetwork);
                networkId = TwitterSocialNetwork.ID;
                logOut("Twitter");
            }


//            if(netId == GooglePlusSocialNetwork.ID){

//                try {

//                    GooglePlusSocialNetwork googlePlusNetwork = new GooglePlusSocialNetwork(this);
//                    mSocialNetworkManager.addSocialNetwork(googlePlusNetwork);
//                    networkId = GooglePlusSocialNetwork.ID;
//                    logOut("GooglePlus");
//                }catch (NoClassDefFoundError e){
//                    logOut(e+"");
//                }


//            }

            if(netId == FacebookSocialNetwork.ID){
                FacebookSocialNetwork facebookNetwork = new FacebookSocialNetwork(
                        this,
                        facebookScope);
                mSocialNetworkManager.addSocialNetwork(facebookNetwork);
                networkId = FacebookSocialNetwork.ID;
                logOut("Facebook");
            }

            if(netId == VkSocialNetwork.ID){
                logOut("VK-key - " + VK_KEY);
                VkSocialNetwork vkontakteNetwork = new VkSocialNetwork(
                        this,
                        VK_KEY,
                        vkontakteScope);
                mSocialNetworkManager.addSocialNetwork(vkontakteNetwork);
                networkId = VkSocialNetwork.ID;
                logOut("Vk");
            }


            getFragmentManager().beginTransaction().add(mSocialNetworkManager, NetworksActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);

    }


    @Override
    public void onSocialNetworkManagerInitialized() {
        logOut("onSocialNetworkManagerInitialized()");
        setOnLoginCompleteListeners();
    }


    private void setOnLoginCompleteListeners(){

        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
        }
        loginRequest();
    }


    private void loginRequest(){

        SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);

        try {

            if (!socialNetwork.isConnected()) {
                if (networkId != 0) {
                    logOut("socialNetwork - " + socialNetwork);
                    socialNetwork.requestLogin();
                    //NetworksActivity.showProgress("Loading social person");
                } else {
                    Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
                }
            } else {
                startProfile(socialNetwork.getID());
            }
        }catch(NullPointerException ex){
            logOut(ex+"");
            //TODO - ������� ������� �� �������� ��������
        }
    }

    @Override
    public void onLoginSuccess(int networkId) {
        logOut("onLoginSuccess()");
        //NetworksActivity.hideProgress();
        startProfile(networkId);
    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        //NetworksActivity.hideProgress();
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    private void startProfile(int networkId){
        ProfileFragment profile = ProfileFragment.newInstannce(networkId);
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("profile")
                .replace(R.id.container, profile)
                .commit();
    }

    public void setNetId(int netId) {
        this.netId = netId;
    }

    private void logOut(String message) {
        Log.d("tag", message);
    }
}
