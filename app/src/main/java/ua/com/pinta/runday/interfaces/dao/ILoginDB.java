package ua.com.pinta.runday.interfaces.dao;


/**
 * Created by Cach on 15.05.2015.
 */
public interface ILoginDB {

    String getUserEmail();

    void writeNewUserEmail(String email);

}
