package ua.com.pinta.runday.classes.run.timetable.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import java.text.DateFormat;
import java.util.Date;
import ua.com.pinta.runday.R;

/**
 * Created by Cach on 11.06.2015.
 */
public class DFragment extends DialogFragment implements View.OnClickListener{



    private Dialog dialog;

    private Date date;

    private TextView data_tv;
    private EditText data_hour_et;
    private EditText data_min_et;
    private ImageView run_iv;
    private ImageView hand_iv;
    private RadioButton run_rb;
    private RadioButton hand_rb;
    private LinearLayout cancel_layout;
    private LinearLayout apply_layout;

    boolean run_b = true;

    public DFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.time_table_popup, container, false);


        data_tv = (TextView) rootView.findViewById(R.id.time_table_data_tv);

        DateFormat dateFormat = DateFormat.getDateInstance();
        data_tv.setText(dateFormat.format(date));

        data_hour_et = (EditText) rootView.findViewById(R.id.time_table_data_hour_et);
        data_min_et = (EditText) rootView.findViewById(R.id.time_table_data_min_et);

        run_iv = (ImageView) rootView.findViewById(R.id.time_table_run_iv);
        hand_iv = (ImageView) rootView.findViewById(R.id.time_table_hand_iv);

        run_rb = (RadioButton) rootView.findViewById(R.id.time_table_run_rb);
        hand_rb = (RadioButton) rootView.findViewById(R.id.time_table_hand_rb);

        cancel_layout = (LinearLayout) rootView.findViewById(R.id.time_table_popup_cancel_layout);
        apply_layout = (LinearLayout) rootView.findViewById(R.id.time_table_popup_apply_layout);

        run_rb.setChecked(true);

        run_rb.setOnClickListener(this);
        hand_rb.setOnClickListener(this);
        cancel_layout.setOnClickListener(this);
        apply_layout.setOnClickListener(this);

        data_hour_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if(s.toString().length() > 2 || Integer.parseInt(s.toString()) > 23) {
                        data_hour_et.setTextColor(getActivity().getResources().getColor(R.color.time_table_popup_warning_color));
                    }else{
                        data_hour_et.setTextColor(getActivity().getResources().getColor(R.color.time_table_popup_text_color));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        data_min_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if(s.toString().length() > 2 || Integer.parseInt(s.toString()) > 59) {
                        data_min_et.setTextColor(getActivity().getResources().getColor(R.color.time_table_popup_warning_color));
                    }else{
                        data_min_et.setTextColor(getActivity().getResources().getColor(R.color.time_table_popup_text_color));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                DisplayMetrics outMetrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(outMetrics);

                dialog.getWindow().setLayout(
                        (int) (outMetrics.widthPixels - getActivity().getResources().getDimension(R.dimen.time_tabe_dialog_margin)),
                        WindowManager.LayoutParams.WRAP_CONTENT);
            }
        });

        return dialog;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.time_table_popup_cancel_layout: {
                dialog.dismiss();
                break;
            }


            case R.id.time_table_popup_apply_layout: {


                int hour = Integer.parseInt(data_hour_et.getText().toString());
                int min = Integer.parseInt(data_min_et.getText().toString());


                //todo - here we set alarm



                boolean run_b = true;


                break;
            }


            case R.id.time_table_run_rb: {
                hand_rb.setChecked(false);
                run_b = true;
                run_iv.setBackgroundResource(R.drawable.beg_popup);
                hand_iv.setBackgroundResource(R.drawable.otdih_popup_nevibran);
                break;
            }


            case R.id.time_table_hand_rb: {
                run_rb.setChecked(false);
                run_b = false;
                run_iv.setBackgroundResource(R.drawable.beg_popup_nevibran);
                hand_iv.setBackgroundResource(R.drawable.otdih_popup);
                break;
            }
        }
    }


    public void setDate(Date date) {
        this.date = date;
    }
}
