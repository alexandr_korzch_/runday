package ua.com.pinta.runday.classes.main.rootcontainer;

import android.util.Log;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.classes.general.checkers.ServiceChecker;
import ua.com.pinta.runday.classes.main.dao.ProfileDB;
import ua.com.pinta.runday.classes.main.listeners.ProfileListener;
import ua.com.pinta.runday.classes.main.listeners.RegistrationListener;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.classes.general.checkers.ServiceReceiver;
import ua.com.pinta.runday.classes.main.listeners.WelcomeListener;
import ua.com.pinta.runday.classes.main.servises.LoginService;
import ua.com.pinta.runday.classes.main.servises.ProfileService;
import ua.com.pinta.runday.classes.main.servises.RegistrationService;
import ua.com.pinta.runday.classes.main.views.ShowLoginView;
import ua.com.pinta.runday.classes.main.views.ShowProfileView;
import ua.com.pinta.runday.classes.main.views.ShowRegistrationView;
import ua.com.pinta.runday.classes.main.views.ShowWelcomeView;
import ua.com.pinta.runday.classes.general.dao.DBHelper;
import ua.com.pinta.runday.classes.main.dao.LoginDB;
import ua.com.pinta.runday.classes.general.files.WorkWithFiles;
import ua.com.pinta.runday.classes.main.views.ViewRouter;
import ua.com.pinta.runday.interfaces.checkers.IServiceChecker;
import ua.com.pinta.runday.interfaces.dao.IProfileDB;
import ua.com.pinta.runday.interfaces.listeners.ILoginListener;
import ua.com.pinta.runday.interfaces.listeners.IProfileListener;
import ua.com.pinta.runday.interfaces.listeners.IRegistrationListener;
import ua.com.pinta.runday.interfaces.checkers.IServiceReceiver;
import ua.com.pinta.runday.interfaces.dao.IDBHelper;
import ua.com.pinta.runday.interfaces.dao.ILoginDB;
import ua.com.pinta.runday.interfaces.files.IWorkWithFiles;
import ua.com.pinta.runday.interfaces.listeners.IWelcomeListener;
import ua.com.pinta.runday.interfaces.servises.ILoginService;
import ua.com.pinta.runday.interfaces.servises.IProfileSaveService;
import ua.com.pinta.runday.interfaces.servises.IRegistrationService;
import ua.com.pinta.runday.interfaces.views.IShow;
import ua.com.pinta.runday.interfaces.views.IViewRouter;


/**
 * Created by alexandr on 16.05.15.
 */
public class BeanContainer {



    private MainActivity activity;

    public BeanContainer(MainActivity activity) {
        if(activity.isLog()) Log.d("tag", "new BeanContainer()");
        this.activity = activity;
    }

    public BeanContainer() {
    }

    //    private Constants constants;
//    public Constants getConstants(boolean newObj) {
//        if(newObj) return new Constants();
//        if(constants == null)constants = new Constants();
//        return constants;
//    }

//    private Options options;
//    public Options getOptions() {
//        if(options == null) options = new Options(activity);
//        return options;
//    }

    private IWorkWithFiles workWithFiles;
    public IWorkWithFiles getWorkWithFiles(boolean newObj) {
        if(newObj) return new WorkWithFiles(activity);
        if(workWithFiles == null)workWithFiles = new WorkWithFiles(activity);
        return workWithFiles;
    }

    private IDBHelper dbHelper;
    public IDBHelper getDbHelper(boolean newObj) {
        if(newObj) return new DBHelper(activity);
        if(dbHelper == null)dbHelper = new DBHelper(activity);
        return dbHelper;
    }

    private ILoginDB loginDB;
    public ILoginDB getLoginDB(boolean newObj) {
        if(newObj) return new LoginDB(activity, (DBHelper) getDbHelper(false));
        if(loginDB == null)loginDB = new LoginDB(activity,
                (DBHelper) getDbHelper(false));
        return loginDB;
    }

    private IServiceChecker serviceChecker;
    public IServiceChecker getServiceChecker(boolean newObj) {
        if(newObj) return new ServiceChecker(activity);
        if(serviceChecker == null)serviceChecker = new ServiceChecker(activity);
        return serviceChecker;
    }

    private IServiceReceiver serviceController;
    public IServiceReceiver getServiceController(boolean newObj) {
        if(newObj) return new ServiceReceiver(activity);
        if(serviceController == null)serviceController = new ServiceReceiver(activity);
        return serviceController;
    }



    private IWelcomeListener welcomeListener;
    public IWelcomeListener getWelcomeListener(boolean newObj) {
        if(newObj) return new WelcomeListener(activity);
        if(welcomeListener == null)welcomeListener = new WelcomeListener(activity);
        return welcomeListener;
    }

    private IRegistrationListener registrationListener;
    public IRegistrationListener getRegistrationListener(boolean newObj) {
        if(newObj) return new RegistrationListener(activity);
        if(registrationListener == null)registrationListener = new RegistrationListener(activity);
        return registrationListener;
    }


    private IRegistrationService registrationService;
    public IRegistrationService getRegistrationService(boolean newObj) {
        if(newObj) return new RegistrationService(activity);
        if(registrationService == null)registrationService = new RegistrationService(activity);
        return registrationService;
    }


    IProfileDB profileDB;
    public IProfileDB getProfileDB(boolean newObj) {
        if(newObj) return new ProfileDB(activity, (DBHelper) getDbHelper(false));
        if(profileDB == null)profileDB = new ProfileDB(activity,
                (DBHelper) getDbHelper(false));
        return profileDB;
    }

    private ILoginListener enterListener;
    public ILoginListener getLoginListener(boolean newObj) {
        if(newObj) return new ua.com.pinta.runday.classes.main.listeners.LoginListener(activity);
        if(enterListener == null)enterListener = new ua.com.pinta.runday.classes.main.listeners.LoginListener(activity);
        return enterListener;
    }

    private ILoginService enterService;
    public ILoginService getLoginService(boolean newObj) {
        if(newObj) return new LoginService(activity);
        if(enterService == null)enterService = new LoginService(activity);
        return enterService;


    }

    private IProfileListener profileListener;
    public IProfileListener getProfileListener(boolean newObj) {
        if(newObj) return new ProfileListener(activity);
        if(profileListener == null)profileListener = new ProfileListener(activity);
        return profileListener;
    }

    private IProfileSaveService profileSaveService;
    public IProfileSaveService getProfileSaveService(boolean newObj) {
        if(newObj) return new ProfileService(activity);
        if(profileSaveService == null)profileSaveService = new ProfileService(activity);
        return profileSaveService;
    }

    private User user;
    public User getUser(boolean newObj) {
        if(newObj) return new User();
        if(user == null)user = new User();
        return user;
    }

    private IViewRouter viewRouter;
    public IViewRouter getViewRouter(boolean newObj) {
        if(newObj) return new ViewRouter(activity);
        if(viewRouter == null)viewRouter = new ViewRouter(activity);
        return viewRouter;
    }

    private ShowWelcomeView showWelcomeView;
    private ShowRegistrationView showRegistrationView;
    private ShowLoginView showEnterView;
    private ShowProfileView showProfile;

    //private ShowPager showPager;

    public IShow getShowView(boolean newObj, String objectName) {

        if(newObj && objectName.equals("welcome")) return new ShowWelcomeView(activity);
        if(newObj && objectName.equals("registration")) return new ShowRegistrationView(activity);
        if(newObj && objectName.equals("enter")) return new ShowLoginView(activity);
        if(newObj && objectName.equals("profile")) return new ShowLoginView(activity);

//        if(newObj && objectName.equals("pager")) return new ShowEnterView(activity);


        if(objectName.equals("enter")){
            if(showEnterView == null)showEnterView = new ShowLoginView(activity);
            return showEnterView;
        }

        if(objectName.equals("registration")){
            if(showRegistrationView == null)showRegistrationView = new ShowRegistrationView(activity);
            return showRegistrationView;
        }

        if(objectName.equals("welcome")){
            if(showWelcomeView == null)showWelcomeView = new ShowWelcomeView(activity);
            return showWelcomeView;
        }



        if(objectName.equals("profile")){
            if(showProfile == null)showProfile = new ShowProfileView(activity);
            return showProfile;
        }

//        if(objectName.equals("pager")){
//            if(showPager == null)showPager = new ShowPager(activity);
//            return showPager;
//        }

        return null;

    }

}
