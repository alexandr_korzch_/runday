package ua.com.pinta.runday.classes.general.checkers;


import android.app.Activity;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.interfaces.checkers.IServiceChecker;


public class ServiceChecker implements IServiceChecker {

    private MainActivity activity;
    public ServiceChecker(MainActivity activity) {
        this.activity = activity;
    }

    public ServiceChecker() {
    }

    @Override
    public boolean isOnline(Activity activity) {

//        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        if (netInfo != null && netInfo.isConnectedOrConnecting()) return true;
//        else return false;

        return true;

    }

    @Override
    public boolean isGooglePlayServices(Activity activity) {

//        GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
//        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
//        if (status != ConnectionResult.SUCCESS) return false;
//        else return true;

        return true;

    }

    @Override
    public boolean isGPS(Activity activity) {
//        LocationManager mlocManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);;
//        return mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return true;
    }


}
