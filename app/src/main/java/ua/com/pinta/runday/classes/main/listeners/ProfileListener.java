package ua.com.pinta.runday.classes.main.listeners;

import android.content.Intent;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

//import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
//import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import ua.com.pinta.runday.GalleryActivity;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.main.models.User;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.interfaces.listeners.IProfileListener;

/**
 * Created by alexandr on 18.05.15.
 */
public class ProfileListener implements IProfileListener {

    MainActivity activity;

    User user;

    ImageView profile_photo_imageView;

    CheckBox profile_man_checkBox;
    CheckBox profile_woman_checkBox;

    EditText download_photo_edit;

    EditText profile_name_edit;
    EditText profile_age_edit;
    EditText profile_height_edit;
    EditText profile_weight_edit;

    EditText profile_country_edit;
    EditText profile_city_edit;

    String sex;



    public ProfileListener(MainActivity activity) {
        Log.d("tag","new ProfileListener()");
        this.activity = activity;
        init();
        checkboxesListeners();
    }

    private void checkboxesListeners(){

        profile_man_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    profile_woman_checkBox.setChecked(false);
                    sex = "male";
                    Log.d("tag", "checkbox_sex - " + sex);
                }
            }
        });

        profile_woman_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) profile_man_checkBox.setChecked(false);
                sex = "female";
                Log.d("tag", "checkbox_sex - " + sex);

            }
        });

    }

    @Override
    public void onClick(View v) {
        Log.d("tag", "onClick(View v)");

        init();

        switch(v.getId()){

            case R.id.download_photo_button:{
                Log.d("tag",v.getId()+"");

                fillUser();


                //save in db

                dialogCameraOrGallery(v);
                break;
            }

            case R.id.profile_save_textView:{
                Log.d("tag", v.getId() + "");

                fillUser();
                saveProfileToDB();
                saveProfileOnService();

                break;
            }
        }
    }




    private void dialogCameraOrGallery(View v) {

        Effectstype effect = Effectstype.Slideright;

        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(activity);

        dialogBuilder
                .withTitle(activity.getString(R.string.foto_popap_title))
                .withTitleColor(activity.getResources().getColor(R.color.title_photo_popap_color))
                .withDividerColor(activity.getResources().getColor(R.color.divider_photo_popap_color))
                .withMessage(activity.getString(R.string.foto_popap_message))
                .withMessageColor(activity.getResources().getColor(R.color.dialog_photo_popap_color))
                .withDialogColor(activity.getResources().getColor(R.color.title_photo_popap_color))
                .withIcon(activity.getResources().getDrawable(R.drawable.foto))
                .isCancelableOnTouchOutside(true)
                .withDuration(700)
                .withEffect(effect)
                .withButton1Text(activity.getString(R.string.foto_popap_camera_bt))
                .withButton2Text(activity.getString(R.string.foto_popap_gallery_bt))
                .setCustomView(R.layout.custom_view, v.getContext())
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        activity.startActivityForResult(intent, MainActivity.CAMERA);
                        dialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent galleryIntent = new Intent(activity, GalleryActivity.class);
                        activity.startActivityForResult(galleryIntent, MainActivity.GALLERY);
                        dialogBuilder.dismiss();

                    }
                })
                .show();
    }

    private void fillUser() {

        Log.d("tag", "fillUser()");

        try {
            user.setFoto(mUserProfile.avatar);
            user.setFirstName(profile_name_edit.getText().toString());
            user.setAge(Integer.parseInt(profile_age_edit.getText().toString()));
            user.setHeight(Integer.parseInt(profile_height_edit.getText().toString()));
            user.setWeight(Integer.parseInt(profile_weight_edit.getText().toString()));

            user.setCountry(profile_country_edit.getText().toString());
            user.setCity(profile_city_edit.getText().toString());

            user.setSex(sex);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }

        Log.d("tag", "mUserProfile.avatar "+ mUserProfile.avatar);
        Log.d("tag", "fillUser()  sex "+ sex);

    }


    private void saveProfileToDB() {

        Log.d("tag","saveProfileToDB()");
        activity.getBeanContainer(false).getProfileDB(false).updateProfileInDB(user);

    }

    private void saveProfileOnService() {
        Log.d("tag","saveProfileOnService()");
        activity.getBeanContainer(false).getProfileSaveService(false).saveProfileFromActivity(user);

    }


    private void init() {
        Log.d("tag","init()");

        user = activity.getBeanContainer(false).getUser(false);

        profile_photo_imageView = (ImageView)activity.findViewById(R.id.profile_photo_imageView);

        profile_man_checkBox = (CheckBox)activity.findViewById(R.id.profile_man_checkBox);
        profile_woman_checkBox = (CheckBox)activity.findViewById(R.id.profile_woman_checkBox);

        download_photo_edit = (EditText)activity.findViewById(R.id.download_photo_edit);
        profile_name_edit = (EditText)activity.findViewById(R.id.profile_name_edit);
        profile_age_edit = (EditText)activity.findViewById(R.id.profile_age_edit);
        profile_height_edit = (EditText)activity.findViewById(R.id.profile_height_edit);
        profile_weight_edit = (EditText)activity.findViewById(R.id.profile_weight_edit);

        profile_country_edit = (EditText)activity.findViewById(R.id.profile_country_edit);
        profile_city_edit = (EditText)activity.findViewById(R.id.profile_city_edit);

    }
}
