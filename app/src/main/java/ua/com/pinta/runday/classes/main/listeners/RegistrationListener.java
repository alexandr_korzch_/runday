package ua.com.pinta.runday.classes.main.listeners;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.interfaces.listeners.IRegistrationListener;
import ua.com.pinta.runday.interfaces.servises.IRegistrationService;


/**
 * Created by alexandr on 16.05.15.
 */
public class RegistrationListener implements IRegistrationListener, TextWatcher {

    MainActivity activity;

    EditText email_reg_edit;
    EditText password_reg_edit;
    EditText confirm_password_edit;

    TextView confirm_password_text;


    public RegistrationListener(MainActivity activity) {
        this.activity = activity;
        if(activity.isLog()) Log.d("tag", "new RegistrationListener()");
        init();
    }


    private void init(){

        if(activity.isLog()) Log.d("tag", "init()");

        confirm_password_text = (TextView)activity.findViewById(R.id.confirm_password_text);

        email_reg_edit = (EditText)activity.findViewById(R.id.email_reg_edit);
        password_reg_edit = (EditText)activity.findViewById(R.id.password_reg_edit);
        confirm_password_edit = (EditText)activity.findViewById(R.id.confirm_password_edit);

        password_reg_edit.addTextChangedListener(this);
        confirm_password_edit.addTextChangedListener(this);

    }

    @Override
    public void onClick(View v) {

        init();

        if(activity.isLog()) Log.d("tag", "onClick()");

        switch(v.getId()){

            case R.id.reset_reg_text:{
                if(activity.isLog()) Log.d("tag", "reset_reg_text");
                cleanEdits();
                break;
            }

            case R.id.save_reg_text:{
                if(activity.isLog()) Log.d("tag", "save_reg_text");
                if(checkPasswordLength()){
                    registrationOnService();
                }else{
                    Toast.makeText(activity, "Пароль должен быть не менее 6 символов",Toast.LENGTH_LONG).show();
                }
                break;
            }

        }

    }


    void registrationOnService(){
        if(activity.isLog()) Log.d("tag", "registrationOnService()");

        IRegistrationService registrationService = activity.getBeanContainer(false).getRegistrationService(false);
        registrationService.registration(
                email_reg_edit.getText().toString(),
                password_reg_edit.getText().toString(),
                confirm_password_edit.getText().toString()
        );
    }


    private boolean checkPasswordLength(){

//        if(password_reg_edit.getText().length() > 5
//                && confirm_password_edit.getText().length() > 5
//                && (password_reg_edit.getText().length() == confirm_password_edit.getText().length()))
        return true;
//        else return false;

    }


    private void cleanEdits(){
        if(activity.isLog()) Log.d("tag", "cleanEdits()");
        email_reg_edit.setText("");
        password_reg_edit.setText("");
        confirm_password_edit.setText("");
    }



    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(activity.isLog()) Log.d("tag", "onTextChanged(CharSequence s, int start, int before, int count)");

        if(password_reg_edit.getText().toString().equals(confirm_password_edit.getText().toString())){
            if(activity.isLog()) Log.d("tag", "Пароли совпадают");
            confirm_password_text.setText("Пароли совпадают");
            confirm_password_text.setTextColor(Color.GREEN);
        }else{
            if(activity.isLog()) Log.d("tag", "Пароли не совпадают");
            confirm_password_text.setText("Пароли не совпадают");
            confirm_password_text.setTextColor(Color.RED);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

}
