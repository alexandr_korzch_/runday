package ua.com.pinta.runday.classes.main.listeners;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
//import com.github.gorbin.asne.googleplus.GooglePlusSocialNetwork;
//import com.github.gorbin.asne.googleplus.GooglePlusSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;

import ua.com.pinta.runday.GooglePlusRegistrActivity;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.NetworksActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.interfaces.listeners.ILoginListener;


/**
 * Created by alexandr on 17.05.15.
 */
public class LoginListener implements ILoginListener {


    MainActivity activity;

    EditText email_enter_edit;
    EditText password_enter_edit;

    public LoginListener(MainActivity activity) {
        if(activity.isLog()) Log.d("tag", "new EnterListener()");
        this.activity = activity;
        init();

    }

    private void init() {
        if(activity.isLog()) Log.d("tag", "init()");
        email_enter_edit = (EditText)activity.findViewById(R.id.email_enter_edit);
        password_enter_edit = (EditText)activity.findViewById(R.id.password_enter_edit);
    }

    @Override
    public void onClick(View v) {
        if(activity.isLog()) Log.d("tag", v.getId()+"");
        init();

        Intent intent = null;

        switch(v.getId()){

            case R.id.rememberpassword_text:{
                //TODO - реализация восстановления пароля, на 17,05,2015 API не готово
                Toast.makeText(activity, "Функция в разработке",Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.enter_text:{

                try {
                    mUserProfile.password = password_enter_edit.getText().toString();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                loginUser();
                break;
            }

            case R.id.facebook_imageView:{
                intent = new Intent(activity, NetworksActivity.class);
                intent.putExtra("networkId", FacebookSocialNetwork.ID);
                break;
            }

            case R.id.twitter_imageView:{
                intent = new Intent(activity, NetworksActivity.class);
                intent.putExtra("networkId", TwitterSocialNetwork.ID);
                break;
            }

            case R.id.google_imageView:{
                intent = new Intent(activity, GooglePlusRegistrActivity.class);
                break;
            }

            case R.id.vk_imageView:{
                intent = new Intent(activity, NetworksActivity.class);
                intent.putExtra("networkId", VkSocialNetwork.ID);
                break;
            }
        }
        if(intent != null)activity.startActivityForResult(intent, MainActivity.REQUEST_NETWORKS);

    }

    private void loginUser() {

        activity.getBeanContainer(false)
                .getLoginService(false)
                .login(
                        email_enter_edit.getText().toString(),
                        password_enter_edit.getText().toString()
                );
    }

}
