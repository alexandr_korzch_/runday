package ua.com.pinta.runday.classes.run.navigationdrawer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import ua.com.pinta.runday.R;
import ua.com.pinta.runday.RunActivity;


public class NavigationDrawerFragment extends Fragment {

    RunActivity activity;
    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;
    private LinearLayout navig_profile;
    private LinearLayout navig_targets;
    private LinearLayout navig_actions;
    private LinearLayout navig_statistic;
    private LinearLayout navig_calories;
    private LinearLayout navig_sittings;

    NavigationListener nav_lis;


    public NavigationDrawerFragment() {

    }


    public void setActivity(RunActivity activity) {
        this.activity = activity;
        nav_lis.setActivity(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_navigation, container, false);

        initComponents(view);

        return view;
    }


    private void initComponents(View view) {

        navig_profile = (LinearLayout)view.findViewById(R.id.navig_profile);
        navig_targets = (LinearLayout)view.findViewById(R.id.navig_targets);
        navig_actions = (LinearLayout)view.findViewById(R.id.navig_actions);
        navig_statistic = (LinearLayout)view.findViewById(R.id.navig_statistic);
        navig_calories = (LinearLayout)view.findViewById(R.id.navig_calories);
        navig_sittings = (LinearLayout)view.findViewById(R.id.navig_sittings);

        nav_lis = new NavigationListener();
        navig_profile.setOnClickListener(nav_lis);
        navig_targets.setOnClickListener(nav_lis);
        navig_actions.setOnClickListener(nav_lis);
        navig_statistic.setOnClickListener(nav_lis);
        navig_calories.setOnClickListener(nav_lis);
        navig_sittings.setOnClickListener(nav_lis);
    }


    public void setUp(DrawerLayout drawerLayout, Toolbar toolBar) {

        this.drawerLayout = drawerLayout;

        actionBarDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),drawerLayout,toolBar,R.string.open,R.string.close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
    }
}