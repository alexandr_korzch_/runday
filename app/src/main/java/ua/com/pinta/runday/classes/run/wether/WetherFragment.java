package ua.com.pinta.runday.classes.run.wether;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.com.pinta.runday.R;


public class WetherFragment extends Fragment {


    private OnFragmentInteractionListener mListener;


    // TODO: Rename and change types and number of parameters
    public static WetherFragment newInstance(String param1, String param2) {
        WetherFragment fragment = new WetherFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    public WetherFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("tag", "onCreateView - WetherFragment");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wether, container, false);
    }

//

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
