package ua.com.pinta.runday.classes.run.actions;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ua.com.pinta.runday.R;


public class ActionsFragment extends Fragment {


    public static ActionsFragment newInstance() {
        ActionsFragment fragment = new ActionsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ActionsFragment() {
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_actions, container, false);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        List<ScamperModel> list = new ArrayList<ScamperModel>();

        for(int i = 0; i<100; i++) {

            ScamperModel scamperModel = new ScamperModel();
            scamperModel.setDate(new Date());
            scamperModel.setDistanse(150);
            scamperModel.setSpeed(12.5f);
            scamperModel.setTime(4561l);

            list.add(scamperModel);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerAdapter mAdapter = new RecyclerAdapter(list);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        return v;
    }



//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
//    }

}