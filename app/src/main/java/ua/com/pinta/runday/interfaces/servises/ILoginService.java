package ua.com.pinta.runday.interfaces.servises;

/**
 * Created by alexandr on 17.05.15.
 */
public interface ILoginService {

    void login(String email, String password);

}
