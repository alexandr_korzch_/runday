package ua.com.pinta.runday.interfaces.dao;

import java.util.List;

import ua.com.pinta.runday.classes.run.map.models.PointOnTrack;

/**
 * Created by Cach on 20.05.2015.
 */
public interface ITrackDB {

    void saveTrack(List<PointOnTrack> points);

    List<PointOnTrack> getLastTrack();

    int getNewTrackNumber();

}
