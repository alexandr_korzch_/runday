package ua.com.pinta.runday.interfaces.checkers;

/**
 * Created by alexandr on 16.05.15.
 */
public interface IServiceReceiver {

    void connectInternet();

    void receiveGooglePlayServices();

    void turnOnGPS();

}
