package ua.com.pinta.runday.interfaces.servises;

import ua.com.pinta.runday.classes.main.models.User;

/**
 * Created by Cach on 19.05.2015.
 */
public interface IProfileSaveService {

    void saveProfileFromActivity(User user);
}
