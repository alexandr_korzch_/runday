package ua.com.pinta.runday.classes.main.views;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.R;
import ua.com.pinta.runday.classes.main.networks.mUserProfile;
import ua.com.pinta.runday.interfaces.listeners.IRegistrationListener;
import ua.com.pinta.runday.interfaces.views.IShow;


/**
 * Created by alexandr on 16.05.15.
 */
public class ShowRegistrationView implements IShow {

    MainActivity activity;

    public ShowRegistrationView(MainActivity activity) {
        this.activity = activity;
        if(activity.isLog()) Log.d("tag", "new ShowRegistrationView()");
    }

    @Override
    public void show() {
        if(activity.isLog()) Log.d("tag", "showRegistration()");
        activity.setContentView(R.layout.registration);

        init();

    }

    void init(){

        TextView reset_reg_text = (TextView)activity.findViewById(R.id.reset_reg_text);
        TextView save_reg_text = (TextView)activity.findViewById(R.id.save_reg_text);

        IRegistrationListener registrationListener = activity.getBeanContainer(false).getRegistrationListener(false);

        reset_reg_text.setOnClickListener(registrationListener);
        save_reg_text.setOnClickListener(registrationListener);


        EditText email_reg_edit = (EditText)activity.findViewById(R.id.email_reg_edit);
        EditText password_reg_edit = (EditText)activity.findViewById(R.id.password_reg_edit);


        if (mUserProfile.email != null && !mUserProfile.email.equals("null")) {
            email_reg_edit.setText(mUserProfile.email);
        }
        if (mUserProfile.password != null && !mUserProfile.password.equals("null")) {
            password_reg_edit.setText(mUserProfile.password);
        }


    }

}
