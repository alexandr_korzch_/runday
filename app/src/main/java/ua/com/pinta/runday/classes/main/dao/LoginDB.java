package ua.com.pinta.runday.classes.main.dao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import ua.com.pinta.runday.MainActivity;
import ua.com.pinta.runday.classes.general.dao.DBHelper;
import ua.com.pinta.runday.interfaces.dao.ILoginDB;


/**
 * Created by Cach on 15.05.2015.
 */
public class LoginDB implements ILoginDB {

    private MainActivity activity;
    private DBHelper dbHelper;
    private SQLiteDatabase database;
    private Cursor cursor;

    public LoginDB(MainActivity activity, DBHelper dbHelper) {
        if(activity.isLog()) Log.d("tag", "LoginDB()");
        this. activity = activity;
        this.dbHelper = dbHelper;

    }

    public LoginDB(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }


    @Override
    public String getUserEmail() {
        Log.d("tag", "getUserEmail()");

        String email = null;

        openDB();
        cursor = database.rawQuery("SELECT * FROM user", null);
        cursor.moveToFirst();
        try {
            email = cursor.getString(cursor.getColumnIndex("email"));
            Log.d("tag", "email "+email);

        }catch(CursorIndexOutOfBoundsException ex){
        }
        //closeDB();
        return email;
    }



    @Override
    public void writeNewUserEmail(String email) {
        if(activity.isLog()) Log.d("tag", "LoginDB writeNewUser(User user)");
        if(activity.isLog()) Log.d("tag", "email - "+ email);
        openDB();
        database.execSQL("DELETE FROM user");
        ContentValues row = new ContentValues();
        row.put("email", email);
        database.insert("user",null, row);
        closeDB();
    }



    private void openDB() throws SQLException {
        Log.d("tag", "openDB()");
        try {
            database = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void closeDB() {
        Log.d("tag", "closeDB()");
        try {
            if (database != null && database.isOpen())
                dbHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
